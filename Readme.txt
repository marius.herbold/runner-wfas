This is the source code of RuNNer version 1.2
developed by Prof. Jörg Behler, Georg-August-Universität Göttingen

This repository is not the official home repository of RuNNer.

This version is made available for use in the workshop
"WORKFLOWS FOR ATOMISTIC SIMULATION" 10-12 March 2021

For obtaining the most recent version of RuNNer and for accessing the developer's version, please contact joerg.behler@uni-goettingen.de



