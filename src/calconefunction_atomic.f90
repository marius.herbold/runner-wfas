!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2020 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! Purpose: calculate symfunction, dsfuncsdxyz and strs for natoms atoms
!! multipurpose subroutine

!! called by:
!! - calcfunctions.f90
!! - getallelectrostatic.f90
!! - getallforces.f90
!! - getallshortforces.f90
!! - optimize_atomic.f90
!! - optimize_short_combined.f90
!! - predictelec.f90
!!
      subroutine calconefunction_atomic(cutoff_type,cutoff_alpha,max_num_neighbors_local,&
        max_num_atoms,n_start,natoms,atomindex,natomsdim,elementindex,&
        maxnum_funcvalues_local,num_funcvalues_local,&
        nelem,zelem,listdim,&
        lsta,lstc,lste,invneighboridx_local,&
        function_type_local,symelement_local,&
        xyzstruct,symfunction,rmin,&
        funccutoff_local,eta_local,rshift_local,lambda_local,&
        zeta_local,dsfuncdxyz,strs,lstb,&
        lperiodic,ldoforces,ldostress,ldohessian_local,lrmin)
!!
      use fileunits
!! don't use globaloptions here
!! don't use symfunctions here
!!
      implicit none
!!
      integer n_start                                            ! in
      integer listdim                                            ! in
      integer cutoff_type                                        ! in
      real*8  cutoff_alpha
      integer maxnum_funcvalues_local                            ! in
      integer num_funcvalues_local(nelem)                        ! in
      integer max_num_atoms                                      ! in
      integer max_num_neighbors_local                            ! in
      integer zelem(max_num_atoms)                               ! in
      integer function_type_local(maxnum_funcvalues_local,nelem) ! in
      integer symelement_local(maxnum_funcvalues_local,2,nelem)  ! in
      integer nelem                                              ! in
      integer lsta(2,max_num_atoms)                              ! in, numbers of neighbors
      integer lstc(listdim)                                      ! in, identification of atom
      integer lste(listdim)                                      ! in, nuclear charge of atom
      integer iindex                                             ! internal
      integer i1,i2,i3,i4,i5                                     ! internal
      integer j
      integer natoms                                             ! in
      integer atomindex(natoms)                                  ! in
      integer natomsdim                                          ! in
      integer jcount                                             ! internal
      integer elementindex(102)                                  ! in
      integer invneighboridx_local(natoms,max_num_atoms)         ! in
!!
      real*8 xyzstruct(3,max_num_atoms)                          ! in
      real*8 symfunction(maxnum_funcvalues_local,natomsdim)      ! out
      real*8 symfunction_temp(maxnum_funcvalues_local)           ! internal 
      real*8 dsfuncdxyz(maxnum_funcvalues_local,natomsdim,0:max_num_neighbors_local,3)  ! out
      real*8 dsfuncdxyz_temp(0:max_num_neighbors_local,3)        ! internal 
      real*8 strs(3,3,maxnum_funcvalues_local,natomsdim)         ! out
      real*8 strs_temp(3,3,maxnum_funcvalues_local)              ! internal 
      real*8 lstb(listdim,4)                                     ! in, xyz and r_ij
      real*8 funccutoff_local(maxnum_funcvalues_local,nelem)     ! in
      real*8 eta_local(maxnum_funcvalues_local,nelem)            ! in
      real*8 rshift_local(maxnum_funcvalues_local,nelem)         ! in
      real*8 lambda_local(maxnum_funcvalues_local,nelem)         ! in
      real*8 zeta_local(maxnum_funcvalues_local,nelem)           ! in
      real*8 rmin                                                ! in

      logical lperiodic                                          ! in
      logical ldoforces                                          ! in
      logical ldostress                                          ! in
      logical ldohessian_local                                   ! in
      logical lrmin                                              ! in/out
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!11
!!    initialization
      symfunction(:,:)   = 0.0d0
      dsfuncdxyz(:,:,:,:)= 0.0d0
      strs(:,:,:,:)      = 0.0d0
      lrmin              = .true. ! default for sufficiently long bonds         
!!
!! check for obviously wrong structures with too short bonds
!! do this only for the atoms of this process here
      do i1=1,natoms
        do i2=lsta(1,i1),lsta(2,i1)
          if(lstb(i2,4).le.rmin)then
            lrmin=.false.
!!            stop
          endif
        enddo
      enddo
!!
!! check for isolated atoms without neighbors in the structure
!! This is in principle allowed, e.g. for gas-surface dynamics, but user should know
      if(lperiodic.or.(natoms.gt.1))then
        do i1=1,natoms
          if(lsta(2,i1).lt.lsta(1,i1))then
!!          if(lsta(1,i1).eq.0)then
!! Error changed to warning, JB 2020/09/17
!!            write(ounit,*)'ERROR: atom ',n_start+i1-1,' has no neighbors'
            write(ounit,*)'WARNING: atom ',n_start+i1-1,' has no neighbors'
!!            stop !'
          endif
        enddo
      endif
      !!
!! calculate the symmetry function values

      !$omp parallel do default(shared) &
      !$omp& shared(symfunction, dsfuncdxyz, strs) &
      !$omp& private(i1, i2, i3, i4, i5, jcount, iindex, dsfuncdxyz_temp, &
      !$omp& symfunction_temp,strs_temp)
      do i1=1,natoms ! loop over all atoms of this process
        jcount = i1 - 1 +  n_start
        iindex=elementindex(zelem(jcount))
        ! todo: strs_temp is never initialized. Is it meant to
        ! accumulate over the i1 iterations? Then this is wrong! 
        strs_temp=0.d0
        do i2=1,num_funcvalues_local(iindex) ! loop over all symmetry functions

          dsfuncdxyz_temp(:,:)=0.0d0
          symfunction_temp(:)=0.0d0

          call getatomsymfunctions(i1,i2,iindex,natoms,atomindex,natomsdim,&
            max_num_atoms,max_num_neighbors_local,&
            invneighboridx_local,jcount,listdim,lsta,lstc,lste,&
            symelement_local,maxnum_funcvalues_local,&
            cutoff_type,cutoff_alpha,nelem,function_type_local,&
            lstb,funccutoff_local,xyzstruct,symfunction_temp,dsfuncdxyz_temp,strs_temp,& 
            eta_local,zeta_local,lambda_local,rshift_local,rmin,&
            ldoforces,ldostress,ldohessian_local)

          symfunction(i2,i1) = symfunction_temp(i2)
         

          ! to avoid race conditions this section can only be accessed
          ! by one thread at a time
          !!$omp critical
          do i3=0,max_num_neighbors_local,1
            do i4=1,3
              dsfuncdxyz(i2,i1,i3,i4)=dsfuncdxyz(i2,i1,i3,i4)+dsfuncdxyz_temp(i3,i4)
              do i5=1,3
                strs(i5,i4,i2,i1)    =strs(i5,i4,i2,i1)+strs_temp(i5,i4,i2)
              enddo ! i5
            enddo ! i4
          enddo ! i3
          !!$omp end critical
        enddo ! i2

        !! Here we dump the derivatives of a given atom for a partical purpose (Emir)
        !do i3=0,max_num_neighbors_local,1
        !   do i4=1,3
        !        write(symderivunit,*) i4,i3,&
        !            (dsfuncdxyz(j,1,i3,i4),j=1,num_funcvalues_local(iindex)) ! dump only for the first atom (specific task !)
        !    end do
        !end do
      enddo ! i1
      !$omp end parallel do


      return
      end
