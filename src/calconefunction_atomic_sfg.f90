!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2020 Prof. Dr. Joerg Behler
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by the
! Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
! for more details.
!
! You should have received a copy of the GNU General Public License along
! with this program. If not, see http://www.gnu.org/licenses.
!######################################################################
!! Purpose: calculate symfunction, dsfuncsdxyz and strs for natoms atoms
!! USING SYMMETRY FUNCTION GROUPS (Emir)
!! multipurpose subroutine

!! called by:
!! - calcfunctions.f90

subroutine calconefunction_atomic_sfg(fc_type,fc_alpha,max_num_neigh_local,&
        max_num_atoms,n_start,n_atoms,atomindex,elementindex,maxnum_funcvalues_local,&
        nelem,zelem,listdim,lsta,lstb,lstc,lste,invneighboridx_local,function_type_local,&
        sf_count_sfg_local,sf_index_sfg,neighbors_sfg_local,maxnum_sfgroups_local,num_groups_local,&
        funccutoff_local,eta_local,rshift_local,lambda_local,zeta_local,xyzstruct,rmin,&
        symfunction,dsfuncdxyz,strs,lperiodic,ldoforces,ldostress,lrmin)

    use fileunits
    use nnflags ! added for the mode check (Emir)

    implicit none

    integer n_start                                            ! in
    integer listdim                                            ! in
    integer fc_type                                            ! in
    real*8  fc_alpha                                           ! in
    integer maxnum_funcvalues_local                            ! in
    integer maxnum_sfgroups_local                              ! in
    integer max_num_atoms                                      ! in
    integer max_num_neigh_local                                ! in
    integer zelem(max_num_atoms)                               ! in
    integer function_type_local(maxnum_sfgroups_local,nelem)   ! in
    integer neighbors_sfg_local(maxnum_sfgroups_local,2,nelem) ! in
    integer num_groups_local(nelem)                            ! in
    integer sf_count_sfg_local(maxnum_sfgroups_local,nelem)    ! in
    integer sf_index_sfg(maxnum_funcvalues_local,maxnum_sfgroups_local,nelem) !in
    integer nelem                                              ! in
    integer lsta(2,max_num_atoms)                              ! in, numbers of neighbors
    integer lstc(listdim)                                      ! in, identification of atom
    integer lste(listdim)                                      ! in, nuclear charge of atom
    integer n_atoms                                            ! in
    integer atomindex(n_atoms)                                 ! in
    integer jcount                                             ! internal
    integer j                                                  ! internal
    integer elementindex(102)                                  ! in
    integer invneighboridx_local(n_atoms,max_num_atoms)        ! in
    integer i1,i2,i3,i4,i5,i6                                  ! in
    integer eindex                                             ! internal
    integer group_type                                         ! internal (0:radial, 1:angular)
    integer group_listdim                                      ! internal
    integer max_group_listdim                                  ! internal
    integer atom_sym_count                                     ! internal
    integer, allocatable :: group_neigh_list(:,:)   ! internal
    integer, allocatable :: group_neigh_idx(:,:)    ! internal
    !!
    real*8, allocatable :: group_radial_distances(:,:) ! internal
    real*8, allocatable :: group_cutoff_values(:,:) ! internal
    real*8, allocatable :: group_cutoff_derivatives(:,:) ! internal
    real*8, allocatable :: group_cutoff_secderivatives_dum(:,:) ! for hessian (mode3)
    real*8 xyzstruct(3,max_num_atoms)                          ! in
    real*8 symfunction(maxnum_funcvalues_local,n_atoms)        ! out
    real*8 symfunction_temp(maxnum_funcvalues_local)           ! internal
    real*8 dsfuncdxyz(maxnum_funcvalues_local,max_num_atoms,0:max_num_neigh_local,3)  ! out
    real*8 dsfuncdxyz_temp(0:max_num_neigh_local,3)        ! internal
    real*8 strs(3,3,maxnum_funcvalues_local,n_atoms)         ! out
    real*8 strs_temp(3,3,maxnum_funcvalues_local)              ! internal
    real*8 lstb(listdim,4)                                     ! in, xyz and r_ij
    real*8 funccutoff_local(maxnum_sfgroups_local,nelem)     ! in
    real*8 eta_local(maxnum_funcvalues_local,maxnum_sfgroups_local,nelem)  ! in
    real*8 rshift_local(maxnum_funcvalues_local,maxnum_sfgroups_local,nelem)         ! in
    real*8 lambda_local(maxnum_funcvalues_local,maxnum_sfgroups_local,nelem)         ! in
    real*8 zeta_local(maxnum_funcvalues_local,maxnum_sfgroups_local,nelem)           ! in
    real*8 rmin                                                ! in

    logical lperiodic                                          ! in
    logical ldoforces                                          ! in
    logical ldostress                                          ! in
    logical lrmin                                              ! in/out
    !!

    !!    initialization
    symfunction(:,:)   = 0.0d0
    dsfuncdxyz(:,:,:,:)= 0.0d0
    strs(:,:,:,:)      = 0.0d0
    lrmin              = .true. ! default for sufficiently long bonds
    max_group_listdim  = 0
    group_listdim = 0

    !! SF derivatives are required in mode 2, make the check here
    if(mode.eq.1)then
        ldoforces = .false.
    elseif(mode.eq.2)then
        ldoforces = .true.
    end if


!! check for obviously wrong structures with too short bonds
!! do this only for the atoms of this process here
    do i1=1,n_atoms
        do i2=lsta(1,i1),lsta(2,i1)
            if(lstb(i2,4).le.rmin)then
                lrmin=.false.
            endif
        enddo
    enddo
!!
!! check for isolated atoms without neighbors in the structure
!! This is in principle allowed, e.g. for gas-surface dynamics, but user should know
      if(lperiodic.or.(n_atoms.gt.1))then
        do i1=1,n_atoms
          if(lsta(2,i1).lt.lsta(1,i1))then
!! Error changed to warning, JB 2020/09/17
!!            if(lsta(1,i1).eq.0)then
!!                write(ounit,*)'ERROR: atom ',n_start+i1-1,' has no neighbors'
            write(ounit,*)'WARNING: atom ',n_start+i1-1,' has no neighbors'
!!                stop !'
          endif
        enddo
      endif


!! Symmetry Function calculations (based on SF groups)
!! First loop : over atoms
!! Second loop : over symmetry function groups
!! Third loop : over symmetry functions in each grop
    do i1 = 1,n_atoms
        jcount = i1 - 1 + n_start
        eindex = elementindex(zelem(jcount))
        atom_sym_count = 1
        do i2 = 1,num_groups_local(eindex)
            call check_type(function_type_local(i2,eindex),group_type)

            ! number of pairs for a particular SFG might be higher than number of neighbors
            max_group_listdim = (lsta(2,i1)-lsta(1,i1)+2)*(lsta(2,i1)-lsta(1,i1)+1)/2

            allocate(group_radial_distances(max_group_listdim,3))
            allocate(group_cutoff_values(max_group_listdim,3))
            allocate(group_cutoff_derivatives(max_group_listdim,3))
            allocate(group_cutoff_secderivatives_dum(max_group_listdim,3))
            allocate(group_neigh_list(max_group_listdim,group_type+1))
            allocate(group_neigh_idx(max_group_listdim,group_type+1))
            group_neigh_list(:,:) = 0
            group_neigh_idx(:,:) = 0
            group_radial_distances(:,:) = 0.d0
            group_cutoff_values(:,:) = 0.d0
            group_cutoff_derivatives(:,:) = 0.d0
            group_cutoff_secderivatives_dum(:,:) = 0.d0

            call getgroupneighlist(lsta(1,i1),lsta(2,i1),lstb,lstc,lste,group_type,function_type_local(i2,eindex)&
                    ,listdim,neighbors_sfg_local(i2,1,eindex),neighbors_sfg_local(i2,2,eindex),&
                    group_listdim,max_group_listdim,group_neigh_list,group_neigh_idx,group_radial_distances)

            call getgroupcutoffvalues(fc_type,fc_alpha,eindex,i2,function_type_local(i2,eindex),&
                    maxnum_sfgroups_local,nelem,funccutoff_local,group_listdim,max_group_listdim,&
                    group_radial_distances,group_cutoff_values,group_cutoff_derivatives,group_cutoff_secderivatives_dum)

            strs_temp = 0.d0 !initialize stress tensor for each group
            do i3 = 1,sf_count_sfg_local(i2,eindex)
                dsfuncdxyz_temp(:,:)=0.0d0
                symfunction_temp(:)=0.0d0

                if(mode.eq.1)then ! calculate SFs just for mode 1, read otherwise

                    call getatomsymfunctions_sfg(i1,i2,i3,eindex,n_atoms,rmin,lrmin,&
                        maxnum_funcvalues_local,maxnum_sfgroups_local,nelem,function_type_local(i2,eindex),&
                        funccutoff_local(i2,eindex),symfunction_temp,eta_local,rshift_local,lambda_local,zeta_local,&
                        group_listdim,max_group_listdim,group_radial_distances,group_cutoff_values)

                        symfunction(sf_index_sfg(i3,i2,eindex),i1) = symfunction_temp(i3)

                end if

                if(ldoforces)then
                    call getsymfunctionderivatives(i1,i2,i3,eindex,jcount,xyzstruct,invneighboridx_local,listdim,&
                            n_atoms,maxnum_funcvalues_local,maxnum_sfgroups_local,max_num_neigh_local,max_num_atoms,&
                            group_neigh_list,group_neigh_idx,group_listdim,max_group_listdim,group_radial_distances,&
                            group_cutoff_values,group_cutoff_derivatives,group_type,nelem,&
                            eta_local,rshift_local,lambda_local,zeta_local,&
                            function_type_local(i2,eindex),funccutoff_local(i2,eindex),lstb,dsfuncdxyz_temp,&
                            ldostress,strs_temp)

                    do i4 = 0,max_num_neigh_local,1
                        do i5 = 1,3
                            dsfuncdxyz(sf_index_sfg(i3,i2,eindex),i1,i4,i5) = &
                                    dsfuncdxyz(sf_index_sfg(i3,i2,eindex),i1,i4,i5) + dsfuncdxyz_temp(i4,i5)
                            if(ldostress)then
                                do i6=1,3
                                    strs(i6,i5,sf_index_sfg(i3,i2,eindex),i1) = &
                                            strs(i6,i5,sf_index_sfg(i3,i2,eindex),i1) + strs_temp(i6,i5,i3)
                                enddo
                            end if
                        enddo
                    enddo
                end if

                atom_sym_count = atom_sym_count + 1

            end do !i3
            ! deallocate group-specific arrays here
            deallocate(group_radial_distances)
            deallocate(group_cutoff_values)
            deallocate(group_cutoff_derivatives)
            deallocate(group_cutoff_secderivatives_dum)
            deallocate(group_neigh_list)
            deallocate(group_neigh_idx)
            group_listdim = 0

        end do !i2
        !! Here we dump the derivatives of a given atom for a partical purpose (Emir)
        !do i4=0,max_num_neigh_local,1
        !    do i5=1,3
        !        write(symderivunit,*) i5,i4,&
        !                (dsfuncdxyz(j,1,i4,i5),j=1,atom_sym_count-1) ! dump only for the first atom (specific task !)
        !    end do
        !end do
    end do !i1

end


