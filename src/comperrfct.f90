!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2020 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
      function comperrfct(x)
!!
      use nnconstants
!!
      implicit none
!!
      real*8 x
      real*8 yy
      real*8 y
      real*8 c1
      real*8 c2
      real*8 c3
      real*8 c4
      real*8 c5
      real*8 c6
      real*8 c7
      real*8 c8
      real*8 c9
      real*8 c10
      real*8 sqrtpi
      real*8 comperrfct
!!
      sqrtpi=dsqrt(pi)
!!
      c1=-1.26551223d0
      c2=1.00002368d0
      c3=0.37409196d0
      c4=0.09678418d0
      c5=-0.18628806d0
      c6=0.27886807d0
      c7=-1.13520398d0
      c8=1.48851587d0
      c9=-0.82215223d0
      c10=0.17087277d0
!!
      y=abs(x)
      yy=1.d0/(1.d0+0.5d0*y)
      comperrfct=yy*exp(-y*y+c1+yy*(c2+yy*&
             (c3+yy*(c4+yy*(c5+yy*(c6+yy*(c7+yy*&
             (c8+yy*(c9+yy*c10)))))))))
      if(x.lt.0.d0)comperrfct=2.d0-comperrfct
!!
      end
