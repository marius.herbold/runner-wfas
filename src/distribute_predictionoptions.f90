!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2018 Prof. Dr. Joerg Behler
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it
! under the terms of the GNU General Public License as published by the
! Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
! for more details.
!
! You should have received a copy of the GNU General Public License along
! with this program. If not, see http://www.gnu.org/licenses.
!######################################################################
!! called by:
!! - main.f90
!!
      subroutine distribute_predictionoptions()
!!
      use mpi_mod
      use predictionoptions
      use globaloptions  !! we need nelem for dimensions
!!
      implicit none
!!
      call mpi_bcast(enforcetotcharge,1,mpi_integer,0,mpi_comm_world,mpierror)
      call mpi_bcast(nn_type_vdw,1,mpi_integer,0,mpi_comm_world,mpierror)
!!
      call mpi_bcast(lpreparemd,1,mpi_logical,0,mpi_comm_world,mpierror)
      call mpi_bcast(ldoforces,1,mpi_logical,0,mpi_comm_world,mpierror)
      call mpi_bcast(ldohessian,1,mpi_logical,0,mpi_comm_world,mpierror)
      call mpi_bcast(lvdw,1,mpi_logical,0,mpi_comm_world,mpierror)
      call mpi_bcast(lprintforcecomponents,1,mpi_logical,0,mpi_comm_world,mpierror)
      call mpi_bcast(lwritesymfunctions,1,mpi_logical,0,mpi_comm_world,mpierror)

      call mpi_bcast(vdw_screening,2,mpi_real8,0,mpi_comm_world,mpierror)
      call mpi_bcast(vdw_coefficients,nelem*nelem,mpi_real8,0,mpi_comm_world,mpierror)

      return
      end
