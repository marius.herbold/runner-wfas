!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2020 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! called by:
!! -main.f90
!!
      subroutine generalinfo()
!!
      use fileunits
!!
      implicit none
!!
      integer nhours,nminutes,day,year,month

!!
!! CHANGE ANDI: GFORTRAN: need return value for gfortran hostnm and getcwd functions
!!
      integer ierr
!! END CHANGE

!!
      real*8 seconds
!!
      character*20 machname
      character*20 hostnm 
      character*60 directory 
      character*60 getcwd 
      character*20 username 
      character*10 date,time
!!
!!      call hostnm(machname,status) ! breda gets trouble with this
!!

!!
!! CHANGE ANDI: GFORTRAN: ifort and gfortran have different functions for machine name and current directory
!!                        to use these preprocessor directives compile this file with -cpp option
!!
     !machname=hostnm()
     !call getlog(username)
     !call date_and_time(date,time)
     !read(time,'(i2,i2,f6.3)')nhours,nminutes,seconds
     !read(date,'(i4,i2,i2)') year,month,day
     !directory=getcwd()
#ifdef __INTEL_COMPILER
      machname=hostnm()
      directory=getcwd()
#else
      ierr=hostnm(machname)
      ierr=getcwd(directory)
#endif

      call getlog(username)
      call date_and_time(date,time)
      read(time,'(i2,i2,f6.3)')nhours,nminutes,seconds
      read(date,'(i4,i2,i2)') year,month,day
!! END CHANGE

!!
      write(ounit,*)'General job information:'
      write(ounit,*)'-------------------------------------------------------------'
      write(ounit,*)'Executing host    : ',machname
      write(ounit,*)'User name         : ',username
      write(ounit,'(a,i2,a,i2,a,i4)')' Starting date     : ',day,'.',month,'.',year
      write(ounit,'(a,i2,a,i2,a,i2)')' Starting time     : ',nhours,' h ',nminutes,' min '
      write(ounit,'(2a)')' Working directory : ',directory
!!      write(ounit,'(4a)')' Job started on ',machname,' by user ',username
!!      write(ounit,'(a,i2,a,i3,2a,i2,a,i2,a,i4)')&
!!        ' Starting at ',nhours,' h ',nminutes,' min ',&
!!        ' on ',day,'.',month,'.',year
!!      write(ounit,'(2a)')' in directory ',directory
!!
      write(ounit,*)'-------------------------------------------------------------'
      return
      end
