!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2020 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! Purpose: calculate nneshort for a set of npoints structures 

!! called by: 
!! - getshortenergies_para.f90
!! - optimize_short_combined.f90
!!
!! very similar to getcharges.f90
!!
      subroutine geteshort(ndim,npoints,&
        zelem_local,num_atoms_local,&
        symfunction_local,nneshort_local_list)
!!
      use fileunits
      use globaloptions
      use nnshort_atomic
!!
      implicit none
!!
      integer ndim                                                    ! in
      integer npoints                                                 ! in
      integer zelem_local(ndim,max_num_atoms)                         ! in
      integer zelem(max_num_atoms)                                    ! internal
      integer num_atoms_local(ndim)                                   ! in
!!
      integer i1
!!
      real*8 symfunction_local(maxnum_funcvalues_short_atomic,max_num_atoms,ndim)  ! in
      real*8 nneshort_local                                           ! internal
      real*8 nneshort_local_list(ndim)                                ! out
      real*8 nnatomenergy(max_num_atoms)                              ! internal
!!
!!
      do i1=1,npoints
!!
        zelem(:)=zelem_local(i1,:)
!! calculate the short-range contribution
        call calconeshort(num_atoms_local(i1),&
          zelem,&
          symfunction_local(1,1,i1),nneshort_local,nnatomenergy)
!!
!! normalize nneshort_local to energy per atom
        nneshort_local=nneshort_local/dble(num_atoms_local(i1))
        nneshort_local_list(i1)=nneshort_local
!!
      enddo ! i1
!!
      return
      end
