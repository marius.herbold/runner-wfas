!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2020 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################

!! called by:
!!
      subroutine getkaldims_elec(&
        kaledim,kalcdim,corredim,corrcdim,&
        maxkaledim,maxcorredim,&
        num_weightsewaldfree)
!!
      use mpi_mod
      use fileunits
      use fittingoptions
      use nnflags
      use globaloptions
!!
      implicit none
!!
      integer num_weightsewaldfree(nelem) ! in
      integer n_start           ! internal, just dummy here
      integer n_end             ! internal, just dummy here
!! Kalman matrix dimensions:
      integer corredim(nelem)   ! out
      integer corrcdim          ! out
      integer kaledim(nelem)    ! out
      integer kalcdim           ! out
      integer i1
      integer isum              ! internal
      integer maxkaledim        ! out
      integer maxcorredim       ! out
!!
!!
!! initializations
      isum=0
!!
!! non-parallel case
      if((mpisize.eq.1).or.lompmkl)then
!!
        do i1=1,nelem
          if(lelec.and.(nn_type_elec.eq.1).and.(optmodeq.eq.1))then
            corredim(i1)=num_weightsewaldfree(i1)*(num_weightsewaldfree(i1)+1)/2
            kaledim(i1)=num_weightsewaldfree(i1)
            if(lchargeconstraint)then
!!              corrcdim=nelem*num_weightsewaldfree*(nelem*num_weightsewaldfree+1)/2
              isum=isum+num_weightsewaldfree(i1)
!!              kalcdim=nelem*num_weightsewaldfree
            endif
          else
            corredim(i1)=1
            kaledim(i1)=1
          endif ! lelec
        enddo ! i1
!!
!! get dimensions for charge constraint
        if(lchargeconstraint.and.lelec)then
          corrcdim=isum*(isum+1)/2
          kalcdim =isum
          isum=0
        else
          corrcdim=1
          kalcdim=1
          isum=0
        endif
!!
      else ! parallel case
!!
        do i1=1,nelem
          if(lelec.and.(nn_type_elec.eq.1).and.(optmodeq.eq.1))then
            call mpifitdistribution(num_weightsewaldfree(i1),kaledim(i1),n_start,n_end)
            corredim(i1)=kaledim(i1)*num_weightsewaldfree(i1)
!! this still needs to be split for the different processes 
            if(lchargeconstraint)then
!!              corrcdim=nelem*num_weightsewaldfree*(nelem*num_weightsewaldfree+1)/2
              isum=isum+num_weightsewaldfree(i1)
!!              kalcdim=nelem*num_weightsewaldfree
            endif
          else
            kaledim(i1)=1
            corredim(i1)=1
          endif ! lelec
        enddo ! i1

        if(lchargeconstraint.and.lelec)then
          corrcdim=isum*(isum+1)/2
          kalcdim =isum
          isum=0
        else
          corrcdim=1
          kalcdim=1
          isum=0
        endif
      endif ! mpisize
!!
      maxkaledim=0
      maxcorredim=0
      do i1=1,nelem
        maxkaledim=max(maxkaledim,kaledim(i1))
        maxcorredim=max(maxcorredim,corredim(i1))
      enddo
!!
      return
      end      
