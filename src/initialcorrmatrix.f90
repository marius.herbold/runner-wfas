!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2020 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! called by:
!! - getkalmanmatrices.f90
!!
      subroutine initialcorrmatrix(num_weights,corrmatrix,noisematrix)
      
      use globaloptions
!!
      implicit none
!!
      integer num_weights                                               ! in
      integer i1,i2                                                     ! internal
      integer icount                                                    ! internal
!!
      real*8 corrmatrix(num_weights*(num_weights+1)/2)  ! out
      real*8 noisematrix(num_weights*(num_weights+1)/2)  ! out and modified by kenko
!!     
      icount=1
!!
      do i1=1,num_weights
        do i2=i1,num_weights
          if(i1.eq.i2)then
            corrmatrix(icount)=1.0d0/kalman_epsilon ! diagonal element
            if(lusenoisematrix)then
              noisematrix(icount)=1.0d0*kalman_q0 ! diagonal element of Noise matrix
            endif 
          else
            corrmatrix(icount)=0.0d0 ! off-diagonal element
            if(lusenoisematrix)then
              noisematrix(icount)=0.0d0 ! off-diagonal element of Noise matrix 
            endif 
          endif
          icount=icount+1
        enddo ! i2
      enddo ! i1
!!
      return
      end
