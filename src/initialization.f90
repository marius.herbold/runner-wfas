!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2020 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! called by:
!! - initnn.f90
!!
      subroutine initialization(ielem,lelement)
!!
      use fileunits
      use nnflags
      use globaloptions
!!
      implicit none
!!
      integer ielem                           ! out number of elements in input.data
!!
      logical lelement(102)                   ! out
!!
!! initializations
      nelem         = 0
      ielem         = 0
      max_num_pairs = 0
!!
!! CHANGE ANDI: GFORTRAN: initialize "lelement" already here, not inside of checkstructures(...).
!!              Otherwise readinput(...) uses uninitialized array in mode 2.
!!
      lelement(:)=.false.
!! END CHANGE
!!
      call checkfiles()
!!
      call getdimensions()
!!
!!     get dimensions for structure-related arrays
      call structurecount()
!!
      call paircount()
!!
!!
!! check structures in input.data for inconsistencies in modes 1 and 3
      if((mode.eq.1).or.(mode.eq.3))then
        call checkstructures(ielem,lelement)
      endif
!!
      return
      end
