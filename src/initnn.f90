!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2020 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! called by: main.f90
!!
      subroutine initnn(iseed)
!!
      use mpi_mod
      use fileunits 
      use fittingoptions
      use mode1options
      use predictionoptions
      use nnflags
      use globaloptions
      use symfunctions
      use symfunctiongroups ! Emir
      use timings
      use nnshort_atomic
      use nnewald
      use nnshort_pair
      use nnconstants
!!
      implicit none
!!
      integer ielem ! number of elements in input.data (just used for checking in initialization part)
      integer*8 iseed ! seed for random numbers
!!
      logical lelement(102)                   ! internal for readinput
!!
      call zerotime(dayinitnn,timeinitnnstart,timeinitnnend)
      call abstime(timeinitnnstart,dayinitnn)
!!
      listdim    =100000 ! preliminary value, is overwritten by better estimate below
!!
      call get_nnconstants()
      call writeheader()
!!
!! check if all processes for parallel run are working
      if(mpisize.gt.1)then
        if(mpirank.eq.0)then
          write(ounit,*)'-------------------------------------------------------------'
          write(ounit,'(a29,i4,a8)')&
            ' Parallel run requested with ',mpisize,' core(s)'
        endif ! mpirank.eq.0
        call mpi_barrier(mpi_comm_world,mpierror)
        write(ounit,*)'Process ',mpirank,' is ready'
        call mpi_barrier(mpi_comm_world,mpierror)
      else
        write(ounit,*)'-------------------------------------------------------------'
        write(ounit,*)'Serial run requested'
      endif
!!
      if(mpirank.eq.0)then
        write(ounit,*)'-------------------------------------------------------------'
      endif ! mpirank.eq.0'
!!
!! analyze input.nn file for the first time to get dimensions of arrays
      if(mpirank.eq.0)then
        call initialization(ielem,lelement)
      endif ! mpirank.eq.0
!!
!! distribute contents of nnflags module 
      call distribute_nnflags()
!!
!! distribute information from initialization
      call mpi_bcast(totnum_structures,1,mpi_integer,0,mpi_comm_world,mpierror)
      call mpi_bcast(max_num_atoms,1,mpi_integer,0,mpi_comm_world,mpierror)
      call mpi_bcast(max_num_pairs,1,mpi_integer,0,mpi_comm_world,mpierror)
      call mpi_bcast(ielem,1,mpi_integer,0,mpi_comm_world,mpierror)
      call mpi_bcast(nelem,1,mpi_integer,0,mpi_comm_world,mpierror)
      call mpi_bcast(npairs,1,mpi_integer,0,mpi_comm_world,mpierror)
      call mpi_bcast(lelement,102,mpi_logical,0,mpi_comm_world,mpierror)
!!
      if(lshort.and.(nn_type_short.eq.1))then
        call mpi_bcast(maxnum_layers_short_atomic,1,mpi_integer,0,mpi_comm_world,mpierror)
        call mpi_bcast(maxnum_funcvalues_short_atomic,1,mpi_integer,0,mpi_comm_world,mpierror)
        call mpi_bcast(maxnodes_short_atomic,1,mpi_integer,0,mpi_comm_world,mpierror)
        if(mode.eq.2)allocate (kalmanlambda(nelem))
        allocate (num_funcvalues_short_atomic(nelem))
        num_funcvalues_short_atomic(:)=0
        allocate (windex_short_atomic(2*maxnum_layers_short_atomic,nelem))
        allocate (num_layers_short_atomic(nelem))
        num_layers_short_atomic(:)=maxnum_layers_short_atomic
        allocate (actfunc_short_atomic(maxnodes_short_atomic,maxnum_layers_short_atomic,nelem))
        allocate (nodes_short_atomic(0:maxnum_layers_short_atomic,nelem))
        nodes_short_atomic(:,:)=0
        allocate (num_weights_short_atomic(nelem))
        num_weights_short_atomic(:)=0
      endif
      if(lshort.and.(nn_type_short.eq.2))then
        call mpi_bcast(maxnum_layers_short_pair,1,mpi_integer,0,mpi_comm_world,mpierror)
        call mpi_bcast(maxnum_funcvalues_short_pair,1,mpi_integer,0,mpi_comm_world,mpierror)
        call mpi_bcast(maxnodes_short_pair,1,mpi_integer,0,mpi_comm_world,mpierror)
        if(mode.eq.2)allocate (kalmanlambdap(npairs))
        allocate (num_funcvalues_short_pair(npairs))
        num_funcvalues_short_pair(:)=0
        allocate (windex_short_pair(2*maxnum_layers_short_pair,npairs))
        allocate (num_layers_short_pair(npairs))
        num_layers_short_pair(:)=maxnum_layers_short_pair
        allocate (actfunc_short_pair(maxnodes_short_pair,maxnum_layers_short_pair,npairs))
        allocate (nodes_short_pair(0:maxnum_layers_short_pair,npairs))
        nodes_short_pair(:,:)=0
        allocate (num_weights_short_pair(npairs))
        num_weights_short_pair(:)=0
      endif
      if(lelec.and.(nn_type_elec.eq.1))then
        call mpi_bcast(maxnum_layers_elec,1,mpi_integer,0,mpi_comm_world,mpierror)
        call mpi_bcast(maxnum_funcvalues_elec,1,mpi_integer,0,mpi_comm_world,mpierror)
        call mpi_bcast(maxnodes_elec,1,mpi_integer,0,mpi_comm_world,mpierror)
        if(mode.eq.2)allocate (kalmanlambdae(nelem))
        allocate (num_funcvalues_elec(nelem))
        num_funcvalues_elec(:)=0
        allocate (windex_elec(2*maxnum_layers_elec,nelem))
        allocate (num_layers_elec(nelem))
        num_layers_elec(:)=maxnum_layers_elec
        allocate (actfunc_elec(maxnodes_elec,maxnum_layers_elec,nelem))
        allocate (nodes_elec(0:maxnum_layers_elec,nelem))
        nodes_elec(:,:)=0
        allocate (num_weights_elec(nelem))
        num_weights_elec(:)=0
      endif
!!'
!! allocate globaloptions
      allocate (fixedcharge(nelem))
      fixedcharge(:)=0.0d0
      allocate (nucelem(nelem))
      allocate (element(nelem))
      allocate (atomrefenergies(nelem))
      allocate (atommasses(nelem))
      allocate (elempair(npairs,2))
      elempair(:,:)=0
!! allocate predictionoptions
      allocate (vdw_coefficients(nelem,nelem))
!!
!! allocate arrays for symmetry functions
      call allocatesymfunctions()
!!
!!======================================================================
!! read input.nn file for the second time, now all keywords are read
!!======================================================================
      call zerotime(dayreadinput,timereadinputstart,timereadinputend) 
      call abstime(timereadinputstart,dayreadinput)
      if(mpirank.eq.0)then
        call readinput(ielem,iseed,lelement)
      endif ! mpirank.eq.0
      call abstime(timereadinputend,dayreadinput)

!! Setup SF groups here after reading all SFs (Emir)
      if (lusesfgroups)then
          call allocatesymfunctiongroups()
          call setupsymfunctiongroups()
          write(ounit,*) 'sf groups are set'
      end if


!!
!! get optimum value for dimension parameter listdim
      call getlistdim()
!!
!! distribute mode 1 options
      if(mode.eq.1)then
        call mpi_bcast(splitthres,1,mpi_real8,0,mpi_comm_world,mpierror)
        call mpi_bcast(fitethres,1,mpi_real8,0,mpi_comm_world,mpierror)
        call mpi_bcast(lfitethres,1,mpi_logical,0,mpi_comm_world,mpierror)
        call mpi_bcast(fitfthres,1,mpi_real8,0,mpi_comm_world,mpierror)
        call mpi_bcast(lfitfthres,1,mpi_logical,0,mpi_comm_world,mpierror)
      endif
!!
!! distribute mode 2 options 
      if(mode.eq.2)then
        call distribute_fittingoptions()
      endif
!!
!! distribute mode 3 options
      if(mode.eq.3)then
        call distribute_predictionoptions()
      endif
!!
!! distribute symmetry function parameters
      call distribute_symfunctions()
!!
!! distribute remaining parameters of globaloptions module
      call distribute_globaloptions()
!!
!! distribute NN specific parameters and arrays
      if(lshort.and.(nn_type_short.eq.1))then
        call mpi_bcast(num_layers_short_atomic,nelem,mpi_integer,0,mpi_comm_world,mpierror)
        call mpi_bcast(nodes_short_atomic,(maxnum_layers_short_atomic+1)*nelem,mpi_integer,0,mpi_comm_world,mpierror)
        call mpi_bcast(windex_short_atomic,2*maxnum_layers_short_atomic*nelem,mpi_integer,0,mpi_comm_world,mpierror)
        call mpi_bcast(num_weights_short_atomic,nelem,mpi_integer,0,mpi_comm_world,mpierror)
        call mpi_bcast(num_funcvalues_short_atomic,nelem,mpi_integer,0,mpi_comm_world,mpierror)
        call mpi_bcast(actfunc_short_atomic,&
          maxnodes_short_atomic*maxnum_layers_short_atomic*nelem,mpi_character,0,mpi_comm_world,mpierror)
        call mpi_bcast(scmin_short_atomic,1,mpi_real8,0,mpi_comm_world,mpierror)
        call mpi_bcast(scmax_short_atomic,1,mpi_real8,0,mpi_comm_world,mpierror)
        allocate (weights_short_atomic(maxnum_weights_short_atomic,nelem))
        weights_short_atomic(:,:)=0.0d0
        allocate (symfunction_short_atomic_list(maxnum_funcvalues_short_atomic,max_num_atoms,nblock))
        symfunction_short_atomic_list(:,:,:)=0.0d0
      endif
!!
      if(lshort.and.(nn_type_short.eq.2))then
        call mpi_bcast(num_layers_short_pair,npairs,mpi_integer,0,mpi_comm_world,mpierror)
        call mpi_bcast(nodes_short_pair,(maxnum_layers_short_pair+1)*npairs,mpi_integer,0,mpi_comm_world,mpierror)
        call mpi_bcast(windex_short_pair,2*maxnum_layers_short_pair*npairs,mpi_integer,0,mpi_comm_world,mpierror)
        call mpi_bcast(num_weights_short_pair,npairs,mpi_integer,0,mpi_comm_world,mpierror)
        call mpi_bcast(num_funcvalues_short_pair,npairs,mpi_integer,0,mpi_comm_world,mpierror)
        call mpi_bcast(actfunc_short_pair,&
          maxnodes_short_pair*maxnum_layers_short_pair*npairs,mpi_character,0,mpi_comm_world,mpierror)
        call mpi_bcast(scmin_short_pair,1,mpi_real8,0,mpi_comm_world,mpierror)
        call mpi_bcast(scmax_short_pair,1,mpi_real8,0,mpi_comm_world,mpierror)
        allocate (weights_short_pair(maxnum_weights_short_pair,npairs))
        weights_short_pair(:,:)=0.0d0
        allocate (symfunction_short_pair_list(maxnum_funcvalues_short_pair,max_num_pairs,nblock))
        symfunction_short_pair_list(:,:,:)=0.0d0
      endif
!!
      if(lelec.and.(nn_type_elec.eq.1))then
        call mpi_bcast(num_layers_elec,nelem,mpi_integer,0,mpi_comm_world,mpierror)
        call mpi_bcast(nodes_elec,(maxnum_layers_elec+1)*nelem,mpi_integer,0,mpi_comm_world,mpierror)
        call mpi_bcast(windex_elec,2*maxnum_layers_elec*nelem,mpi_integer,0,mpi_comm_world,mpierror)
        call mpi_bcast(num_weights_elec,nelem,mpi_integer,0,mpi_comm_world,mpierror)
        call mpi_bcast(num_funcvalues_elec,nelem,mpi_integer,0,mpi_comm_world,mpierror)
        call mpi_bcast(actfunc_elec,&
          maxnodes_elec*maxnum_layers_elec*nelem,mpi_character,0,mpi_comm_world,mpierror)
        call mpi_bcast(scmin_elec,1,mpi_real8,0,mpi_comm_world,mpierror)
        call mpi_bcast(scmax_elec,1,mpi_real8,0,mpi_comm_world,mpierror)
        allocate (weights_elec(maxnum_weights_elec,nelem))
        weights_elec(:,:)=0.0d0
        allocate (symfunction_elec_list(maxnum_funcvalues_elec,max_num_atoms,nblock))
        symfunction_elec_list(:,:,:)=0.0d0
      endif
!!
!! distribute the rest
      call mpi_bcast(ielem,1,mpi_integer,0,mpi_comm_world,mpierror)
      call mpi_bcast(iseed,1,mpi_real8,0,mpi_comm_world,mpierror)
!!
      call abstime(timeinitnnend,dayinitnn)
!!
      return
      end
