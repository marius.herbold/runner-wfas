!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2020 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################!!
      program RuNNer
!!
      use fileunits 
      use nnflags
      use globaloptions
      use mpi_mod
      use timings
!!
      implicit none
!!
      integer*8 iseed ! seed for random numbers
!!
!!======================================================================
!! prepare mpi
!!======================================================================
      call mpi_init(mpierror)
      if(mpierror.ne.0)then
        write(ounit,*)'Error in mpi_init ',mpierror
        stop
      endif 
!! get number of processes mpisize
      call mpi_comm_size(mpi_comm_world,mpisize,mpierror)
!! get process id mpirank
      call mpi_comm_rank(mpi_comm_world,mpirank,mpierror)
!!
!!======================================================================
!! prepare timer
!!======================================================================
      call zerotime(runtimeday,runtimestart,runtimeend)
      call abstime(runtimestart,runtimeday)
!!
!!======================================================================
!! initialization
!!======================================================================
      call initnn(iseed)
!!
!!======================================================================
!! mode 1: Generate symmetry functions
!!======================================================================
      if(mode.eq.1)then
        call mode1(iseed)
!!
!!======================================================================
!! mode 2: Fitting 
!!======================================================================
      elseif(mode.eq.2)then
        call mode2(iseed)
!!
!!======================================================================
!! mode 3: Prediction 
!!======================================================================
      elseif(mode.eq.3)then
          call predict()
!!
      else
        write(ounit,*)'Error: unknown RuNNer mode ',mode
        stop
      endif
!!
!!======================================================================
!! final cleanup, deallocations, print summary
!!======================================================================
      call abstime(runtimeend,runtimeday)
      call cleanup()
!!
!!======================================================================
!! shutdown mpi
!!======================================================================
      call mpi_finalize(mpierror)
      if(mpierror.ne.0)then
        write(ounit,*)'mpierror finalize ',mpierror
      endif
      end
