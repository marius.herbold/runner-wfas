!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2020 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################!!
!! called by:
!! - main.f90
!!
      subroutine mode1(iseed)
!!
      use mpi_mod
      use fileunits
      use nnflags
      use globaloptions
      use mode1options
      use nnshort_atomic
      use nnshort_pair
      use nnewald
      use symfunctions
      use structures
      use timings
!!
      implicit none
!!
      integer*8 iseed                                                        ! in/out
      integer numtrain                                                     ! internal 
      integer numtest                                                      ! internal
      integer numrej                                                       ! internal 
      integer ndim                                                         ! internal 
      integer pointnumber                                                  ! internal
      integer i1,i2                                                        ! internal
      character*200 :: filenametemp                                        ! internal


!!
!!===================================================================================
!! measure total time of mode 1 
!!===================================================================================
      call zerotime(daymode1,timemode1start,timemode1end) 
      call abstime(timemode1start,daymode1)
!!
!!===================================================================================
!! do everything in this subroutine only for non-parallel case
!!===================================================================================
      if(mpisize.gt.1)then
        write(ounit,*)'ERROR: mode 1 is implemented only for non-parallel case'
        stop !'
      endif
!!
!!===================================================================================
!! write output 
!!===================================================================================
      write(ounit,*)'Maximum number of atoms: ',max_num_atoms
!! JB 2019/05/08: write maximum number of pairs only for nn_type_short 2 (otherwise it is not computed, and a computation may not be good because of array dimensions)
      if(nn_type_short.eq.2)then
        write(ounit,*)'Maximum number of pairs: ',max_num_pairs
      endif
      write(ounit,*)'-------------------------------------------------------------'
      write(ounit,*)'Calculating Symmetry Functions'
      write(ounit,*)'for ',totnum_structures,' structures'
      write(ounit,*)'-------------------------------------------------------------'
!!===================================================================================
!!'
!!===================================================================================
!!  allocate arrays of structures module for a set of nblock structures
!!===================================================================================
      call allocatestructures()
!!
!!===================================================================================
!! initializations
!!===================================================================================
      numtrain                = 0
      numtest                 = 0
      numrej                  = 0
      pointnumber             = 0
!!
!!===================================================================================
!! consistency check if the number of function values  
!! the same as input nodes in input.nn
!!===================================================================================
      if(lshort.and.(nn_type_short.eq.1))then
        do i1=1,nelem
          if(num_funcvalues_short_atomic(i1).ne.nodes_short_atomic(0,i1))then
            write(ounit,*)'ERROR: num_funcvalues inconsistent with nodes_short_atomic ',element(i1)
            stop !'
          endif
        enddo ! i1
      endif
      if(lshort.and.(nn_type_short.eq.2))then
        do i1=1,npairs
          if(num_funcvalues_short_pair(i1).ne.nodes_short_pair(0,i1))then
            write(ounit,*)'ERROR: num_funcvalues inconsistent with nodes_short_pair ',i1
            stop !'
          endif
        enddo ! i1
      endif
      if(lelec.and.(nn_type_elec.eq.1))then
        do i1=1,nelem
          if(num_funcvalues_elec(i1).ne.nodes_elec(0,i1))then
            write(ounit,*)'ERROR: num_funcvalues inconsistent with nodes_elec ',element(i1)
            stop !'
          endif
        enddo ! i1
      endif

!!
!!===================================================================================
!! determine maxcutoffs 
!!===================================================================================
      maxcutoff_short_atomic  = 0.0d0
      maxcutoff_short_pair    = 0.0d0
      maxcutoff_elec          = 0.0d0

      if(lshort.and.(nn_type_short.eq.1))then
        do i1=1,nelem
          do i2=1,num_funcvalues_short_atomic(i1)
            maxcutoff_short_atomic=max(maxcutoff_short_atomic,funccutoff_short_atomic(i2,i1))
          enddo ! i2
        enddo ! i1
      elseif(lshort.and.(nn_type_short.eq.2))then
        do i1=1,npairs
          do i2=1,num_funcvalues_short_pair(i1)
            maxcutoff_short_pair=max(maxcutoff_short_pair,funccutoff_short_pair(i2,i1))
          enddo ! i2
        enddo ! i1
      endif
      if(lelec.and.(nn_type_elec.eq.1))then
        do i2=1,nelem
          do i1=1,num_funcvalues_elec(i2)
            maxcutoff_elec=max(maxcutoff_elec,funccutoff_elec(i1,i2))
          enddo ! i1
        enddo ! i2
      endif
!!
!!===================================================================================
!! open files
!!===================================================================================

      open(dataunit,file='input.data',form='formatted')
      rewind(dataunit)
      open(trainstructunit,file='trainstruct.data',form='formatted',status='replace')
      rewind(trainstructunit)
      open(teststructunit,file='teststruct.data',form='formatted',status='replace')
      rewind(teststructunit)
      if(lshort)then
        open(symunit,file='function.data',form='formatted',status='replace')
        rewind(symunit)
        open(tymunit,file='testing.data',form='formatted',status='replace')
        rewind(tymunit)
        open(trainfunit,file='trainforces.data',form='formatted',status='replace')
        rewind(trainfunit)
        open(testfunit,file='testforces.data',form='formatted',status='replace')
        rewind(testfunit)
      endif
      if(lelec.and.(nn_type_elec.eq.1))then ! if we train a separate charge NN:
        open(symeunit,file='functione.data',form='formatted',status='replace')
        rewind(symeunit)
        open(tymeunit,file='testinge.data',form='formatted',status='replace')
        rewind(tymeunit)
        open(trainfeunit,file='trainforcese.data',form='formatted',status='replace')
        rewind(trainfeunit)
        open(testfeunit,file='testforcese.data',form='formatted',status='replace')
        rewind(testfeunit) !'
      endif
      !! Dump SF derivatives in mode 1 if required for a specific analysis (default false) - Emir
      lwritederiv = .false.
      if(lshort.and.lwritederiv)then
          open(symderivunit,file='dsfuncdxyz.data',form='formatted',status='replace')
          rewind(symderivunit)
      end if
!!
!!===================================================================================
!! calculate and write the symmetry functions 
!!===================================================================================
      call getsymmetryfunctions(iseed,numtrain,numtest,numrej)
!!
!!===================================================================================
!! close files
!!===================================================================================
      close(dataunit)
      close(trainstructunit)
      close(teststructunit)
      if(lshort)then
        close(symunit)
        close(tymunit)
        close(trainfunit)
        close(testfunit)
      endif
      if(lelec.and.(nn_type_elec.eq.1))then
        close(symeunit)
        close(tymeunit)
        close(trainfeunit)
        close(testfeunit)
      endif
      !! Dump SF derivatives in mode 1 if required for a specific analysis (default false) - Emir
      if(lshort.and.lwritederiv)then
        close(symderivunit)
      end if
!!
!!===================================================================================
!! write summary to output file
!!===================================================================================
      write(ounit,*)'-------------------------------------------------------------'
      write(ounit,*)'Number of fitting points: ',numtrain
      write(ounit,*)'Number of testing points: ',numtest
      write(ounit,*)'Number of rejected points:',numrej
!! '
!!===================================================================================
!!    deallocate arrays of structures module
!!===================================================================================
      call deallocatestructures()
!!
      call abstime(timemode1end,daymode1)
!!
      return
      end
