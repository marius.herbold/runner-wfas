!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2020 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! Purpose: - determine max_num_pairs
!!          - check if pair symmetry functions are given in input.nn in case of nn_type_short 2
!!          - please note that for maxcutoff we do not distinguish between short range and electrostatic here
!!
!! called by:
!! - initialization.f90
!! 
      subroutine paircount()
!!
      use fileunits
      use nnflags
      use globaloptions
!!
      implicit none
!!
      integer count_struct                     ! internal
      integer function_type_temp               ! internal
      integer i1,i2                            ! internal
      integer nlattice                         ! internal
      integer num_atoms                        ! internal
      integer nn_type_short_local                    ! internal
      integer nn_type_elec_local
      integer num_pairs                        ! in
      integer zelem(max_num_atoms)             ! internal 

      real*8  funccutoff_local                 ! internal 
      real*8  lattice(3,3)                     ! internal
      real*8  maxcutoff_local                  ! internal 
      real*8  xyzstruct(3,max_num_atoms)       ! internal
      real*8  dmin_temp(nelem*(nelem+1)/2)     ! internal
!!
      character*2  elementsymbol(max_num_atoms)! internal 
      character*2  elementtemp1                ! internal
      character*2  elementtemp2                ! internal
      character*2  elementtemp3                ! internal
      character*40 keyword                     ! internal
      character*40 dummy
      character*7  dummy1
      character*40 dummy2
      logical lperiodic                        ! internal
!!
!! initializations:

      max_num_pairs    = 0
      maxcutoff_local  = 0.0d0
      funccutoff_local = 0.0d0
      nlattice         = 0
      num_atoms        = 0
      nn_type_short_local    = 0  ! use the same default as in readinput.f90 here
      nn_type_elec_local = 0
      dmin_element(:)  = 11000.d0

!! read input.nn file  to get the maxcutoff_local
      open(nnunit,file='input.nn')
      rewind(nnunit)
90    read(nnunit,*,END=80) keyword

      if(keyword.eq.'nn_type_short')then
        backspace(nnunit)
        read(nnunit,*)dummy1,nn_type_short_local
      endif
      if((keyword.eq.'electrostatic_type').or.(keyword.eq.'nn_type_elec'))then
        backspace(nnunit)
        read(nnunit,*)dummy2,nn_type_elec_local
      endif
      goto 90
80    continue
      close(nnunit)

      if(nn_type_short_local.eq.2)then
        open(nnunit,file='input.nn')
        rewind(nnunit)

70      read(nnunit,*,END=60) keyword

          if(keyword.eq.'global_pairsymfunction_short')then
            backspace(nnunit)
            read(nnunit,*)dummy,function_type_temp
            if(function_type_temp.eq.1)then
              backspace(nnunit)
              read(nnunit,*)dummy,function_type_temp,funccutoff_local
            elseif(function_type_temp.eq.2)then
              backspace(nnunit)
              read(nnunit,*)dummy,function_type_temp,funccutoff_local
            elseif(function_type_temp.eq.3)then   
              backspace(nnunit)
              read(nnunit,*)dummy,function_type_temp,dummy,dummy,funccutoff_local
            elseif(function_type_temp.eq.4)then   
              backspace(nnunit)
              read(nnunit,*)dummy,function_type_temp,dummy,dummy,dummy,funccutoff_local
            elseif(function_type_temp.eq.5)then    
              backspace(nnunit)
              read(nnunit,*)dummy,function_type_temp,funccutoff_local
            elseif(function_type_temp.eq.6)then   
              backspace(nnunit)
              read(nnunit,*)dummy,function_type_temp,dummy,dummy,funccutoff_local
            else
              write(ounit,*)'Error: unknown symfunction in paircount'
              stop
            endif
!!
          elseif(keyword.eq.'element_pairsymfunction_short')then
            backspace(nnunit)
            read(nnunit,*)dummy,elementtemp1,elementtemp2,function_type_temp
            if(function_type_temp.eq.1)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,elementtemp2,function_type_temp,funccutoff_local
            elseif(function_type_temp.eq.2)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,elementtemp2,function_type_temp,funccutoff_local
            elseif(function_type_temp.eq.3)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,elementtemp2,function_type_temp,dummy,dummy,funccutoff_local
            elseif(function_type_temp.eq.4)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,elementtemp2,function_type_temp,dummy,dummy,dummy,funccutoff_local
            elseif(function_type_temp.eq.5)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,elementtemp2,function_type_temp,funccutoff_local
            elseif(function_type_temp.eq.6)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,elementtemp2,function_type_temp,dummy,dummy,funccutoff_local
            else
              write(ounit,*)'Error: unknown symfunction in paircount'
              stop
           endif
!!
          elseif(keyword.eq.'pairsymfunction_short')then       
            backspace(nnunit)
            read(nnunit,*)dummy,elementtemp1,elementtemp2,function_type_temp
            if(function_type_temp.eq.1)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,elementtemp2,function_type_temp,dummy,funccutoff_local
            elseif(function_type_temp.eq.2)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,elementtemp2,function_type_temp,dummy,funccutoff_local
            elseif(function_type_temp.eq.3)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,elementtemp2,function_type_temp,dummy,dummy,dummy,funccutoff_local
            elseif(function_type_temp.eq.4)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,elementtemp2,function_type_temp,dummy,dummy,dummy,dummy,funccutoff_local
            elseif(function_type_temp.eq.5)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,elementtemp2,function_type_temp,funccutoff_local
            elseif(function_type_temp.eq.6)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,elementtemp2,function_type_temp,dummy,dummy,funccutoff_local
            else
              write(ounit,*)'Error: unknown symfunction in paircount'
              stop
           endif
          endif
!!
          maxcutoff_local=max(maxcutoff_local,funccutoff_local)
          goto 70
60        continue
        close(nnunit)

!! check if maxcutoff_local has been found properly
        if(maxcutoff_local.eq.0.0d0)then
          write(ounit,*)'Error: maxcutoff_local is zero in paircount'
          write(ounit,*)'Did you forget to specify symmetry functions???'
          stop !'
        endif
      endif ! --> nn_type_short_local = 2

!! get the cutoff for nn_type_short_local = 1
      if(nn_type_short_local.eq.1)then
        open(nnunit,file='input.nn')
        rewind(nnunit)

110      read(nnunit,*,END=100) keyword

          if((keyword.eq.'global_symfunction_short').or.&
             (keyword.eq.'global_symfunction_short_atomic'))then
            backspace(nnunit)
            read(nnunit,*)dummy,function_type_temp
            if(function_type_temp.eq.1)then
              backspace(nnunit)
              read(nnunit,*)dummy,function_type_temp,funccutoff_local
            elseif(function_type_temp.eq.2)then
              backspace(nnunit)
              read(nnunit,*)dummy,function_type_temp,dummy,dummy,funccutoff_local
            elseif(function_type_temp.eq.3)then   
              backspace(nnunit)
              read(nnunit,*)dummy,function_type_temp,dummy,dummy,dummy,funccutoff_local
            elseif(function_type_temp.eq.4)then   
              backspace(nnunit)
              read(nnunit,*)dummy,function_type_temp,dummy,funccutoff_local
            elseif(function_type_temp.eq.5)then 
!! just let this debug function pass  
            elseif(function_type_temp.eq.6)then   
!! just let this debug function pass  
            elseif(function_type_temp.eq.8)then    
              backspace(nnunit)
              read(nnunit,*)dummy,function_type_temp,dummy,dummy,funccutoff_local
            elseif(function_type_temp.eq.9)then   
              backspace(nnunit)
              read(nnunit,*)dummy,function_type_temp,dummy,dummy,dummy,funccutoff_local
            else
              write(ounit,*)'Error: unknown symfunction in paircount'
              stop
            endif
!!
          elseif(keyword.eq.'element_symfunction_short')then
            backspace(nnunit)
            read(nnunit,*)dummy,elementtemp1,function_type_temp
            if(function_type_temp.eq.1)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,function_type_temp,&
                funccutoff_local
            elseif(function_type_temp.eq.2)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,function_type_temp,&
                dummy,dummy,funccutoff_local
            elseif(function_type_temp.eq.3)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,function_type_temp,&
                dummy,dummy,dummy,funccutoff_local
            elseif(function_type_temp.eq.4)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,function_type_temp,&
                dummy,funccutoff_local
            elseif(function_type_temp.eq.5)then 
!! just let this debug function pass  
            elseif(function_type_temp.eq.6)then   
!! just let this debug function pass  
            elseif(function_type_temp.eq.8)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,function_type_temp,&
                dummy,dummy,funccutoff_local
            elseif(function_type_temp.eq.9)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,function_type_temp,&
                dummy,dummy,dummy,funccutoff_local
            else
              write(ounit,*)'Error: unknown symfunction in paircount'
              stop
           endif
!!
          elseif(keyword.eq.'symfunction_short')then       
            backspace(nnunit)
            read(nnunit,*)dummy,elementtemp1,function_type_temp
            if(function_type_temp.eq.1)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,function_type_temp,&
                elementtemp2,funccutoff_local
            elseif(function_type_temp.eq.2)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,function_type_temp,&
                elementtemp2,dummy,dummy,funccutoff_local
            elseif(function_type_temp.eq.3)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,function_type_temp,&
                elementtemp2,elementtemp3,dummy,dummy,dummy,funccutoff_local
            elseif(function_type_temp.eq.4)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,function_type_temp,&
                elementtemp2,dummy,funccutoff_local
            elseif(function_type_temp.eq.5)then 
!! just let this debug function pass  
            elseif(function_type_temp.eq.6)then   
!! just let this debug function pass  
            elseif(function_type_temp.eq.8)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,function_type_temp,&
                elementtemp2,elementtemp3,dummy,dummy,funccutoff_local
            elseif(function_type_temp.eq.9)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,function_type_temp,&
                elementtemp2,elementtemp3,dummy,dummy,dummy,funccutoff_local
            else
              write(ounit,*)'Error: unknown symfunction in paircount'
              stop
           endif
          endif
!!
          maxcutoff_local=max(maxcutoff_local,funccutoff_local)
          goto 110
100        continue
        close(nnunit)

!! check if maxcutoff_local has been found properly
        if(maxcutoff_local.eq.0.0d0)then
          write(ounit,*)'Error: maxcutoff_local is zero in paircount'
          write(ounit,*)'Did you forget to specify symmetry functions???'
          stop !'
        endif
      endif ! --> nn_type_short_local = 1

!! get the cutoff for all electrostatic type
      if((nn_type_elec_local.eq.1) .or. (nn_type_elec_local.eq.3) .or. (nn_type_elec_local.eq.4) )then
        open(nnunit,file='input.nn')
        rewind(nnunit)

130      read(nnunit,*,END=120) keyword

          if(keyword.eq.'global_symfunction_electrostatic')then
            backspace(nnunit)
            read(nnunit,*)dummy,function_type_temp
            if(function_type_temp.eq.1)then
              backspace(nnunit)
              read(nnunit,*)dummy,function_type_temp,funccutoff_local
            elseif(function_type_temp.eq.2)then
              backspace(nnunit)
              read(nnunit,*)dummy,function_type_temp,dummy,dummy,funccutoff_local
            elseif(function_type_temp.eq.3)then
              backspace(nnunit)
              read(nnunit,*)dummy,function_type_temp,dummy,dummy,dummy,funccutoff_local
            elseif(function_type_temp.eq.4)then
              backspace(nnunit)
              read(nnunit,*)dummy,function_type_temp,dummy,funccutoff_local
            elseif(function_type_temp.eq.8)then
              backspace(nnunit)
              read(nnunit,*)dummy,function_type_temp,dummy,dummy,funccutoff_local
            elseif(function_type_temp.eq.9)then
              backspace(nnunit)
              read(nnunit,*)dummy,function_type_temp,dummy,dummy,dummy,funccutoff_local
            else
              write(ounit,*)'Error: unknown symfunction in paircount'
              stop
            endif
!!
          elseif(keyword.eq.'element_symfunction_electrostatic')then
            backspace(nnunit)
            read(nnunit,*)dummy,elementtemp1,function_type_temp
            if(function_type_temp.eq.1)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,function_type_temp,&
                funccutoff_local
            elseif(function_type_temp.eq.2)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,function_type_temp,&
                dummy,dummy,funccutoff_local
            elseif(function_type_temp.eq.3)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,function_type_temp,&
                dummy,dummy,dummy,funccutoff_local
            elseif(function_type_temp.eq.4)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,function_type_temp,&
                dummy,funccutoff_local
            elseif(function_type_temp.eq.8)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,function_type_temp,&
                dummy,dummy,funccutoff_local
            elseif(function_type_temp.eq.9)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,function_type_temp,&
                dummy,dummy,dummy,funccutoff_local
            else
              write(ounit,*)'Error: unknown symfunction in paircount'
              stop
           endif

!!
          elseif(keyword.eq.'symfunction_electrostatic')then
            backspace(nnunit)
            read(nnunit,*)dummy,elementtemp1,function_type_temp
            if(function_type_temp.eq.1)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,function_type_temp,&
                elementtemp2,funccutoff_local
            elseif(function_type_temp.eq.2)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,function_type_temp,&
                elementtemp2,dummy,dummy,funccutoff_local
            elseif(function_type_temp.eq.3)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,function_type_temp,&
                elementtemp2,elementtemp3,dummy,dummy,dummy,funccutoff_local
            elseif(function_type_temp.eq.4)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,function_type_temp,&
                elementtemp2,dummy,dummy,funccutoff_local
            elseif(function_type_temp.eq.8)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,function_type_temp,&
                elementtemp2,elementtemp3,dummy,dummy,funccutoff_local
            elseif(function_type_temp.eq.9)then
              backspace(nnunit)
              read(nnunit,*)dummy,elementtemp1,function_type_temp,&
                elementtemp2,elementtemp3,dummy,dummy,dummy,funccutoff_local
            else
              write(ounit,*)'Error: unknown symfunction in paircount'
              stop
           endif
          endif
!!
          maxcutoff_local=max(maxcutoff_local,funccutoff_local)
          goto 130
120        continue
        close(nnunit)

!! check if maxcutoff_local has been found properly
        if(maxcutoff_local.eq.0.0d0)then
          write(ounit,*)'Error: maxcutoff_local is zero in paircount'
          write(ounit,*)'Did you forget to specify symmetry functions???'
          stop !'
        endif
      endif ! --> nn_type_elec_lcoal = 1,3,4

!! Determine max_num_pairs:
!! JB 2019/07/11 This should now always be done to determine
!! dmin_element
!! to avoid unnecessary memory allocation by max_num_pairs, we set it to
!zero if not needed below      
!!      if(nn_type_short_local.eq.2)then
        count_struct=0
        open(dataunit,file='input.data',form='formatted')
          backspace(dataunit)
 10       continue
          read(dataunit,*,END=30) keyword
          if(keyword.eq.'begin') then
            count_struct= count_struct+1
            nlattice  = 0
            num_atoms = 0
            lperiodic =.false.
          endif
          if(keyword.eq.'lattice') then
            nlattice=nlattice+1
            backspace(dataunit)
            read(dataunit,*)keyword,(lattice(nlattice,i1),i1=1,3)
          endif
          if(keyword.eq.'atom') then
            backspace(dataunit)
            num_atoms=num_atoms+1
            read(dataunit,*)keyword,(xyzstruct(i1,num_atoms),i1=1,3),elementsymbol(num_atoms)
            call nuccharge(elementsymbol(num_atoms),zelem(num_atoms))
          endif
          if(keyword.eq.'end') then
            if(nlattice.eq.3)then
              lperiodic=.true.
            endif 
            if(lperiodic)then
               call translate(num_atoms,lattice,xyzstruct)
            endif
!! determine num_pairs
            call getnumpairs(num_atoms,num_pairs,zelem,&
              maxcutoff_local,lattice,xyzstruct,dmin_temp,lperiodic)
            max_num_pairs=max(max_num_pairs,num_pairs)
            do i1=1,nelem*(nelem+1)/2
!!              write(ounit,*)i1,dmin_element(i1),dmin_temp(i1)
              dmin_element(i1)=min(dmin_element(i1),dmin_temp(i1))
            enddo
!!          
          endif
          goto 10
 30       continue
        close(dataunit)
!!      endif ! nn_type_short_local.eq.2     
      if(nn_type_short_local.ne.2)then
        max_num_pairs=0
!! save memory
      endif

      if((max_num_pairs.eq.0).and.(nn_type_short_local.eq.2))then
        write(ounit,*)'Error: max_num_pairs is 0 in paircount.f90 ',max_num_pairs
        stop
      endif
!! debug output
!!      write(ounit,*)'paircount: dmin_element'
!!      write(ounit,*)(dmin_element(i1),i1=1,nelem*(nelem+1)/2)
!!
      return
      end
