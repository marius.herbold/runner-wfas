!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2020 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! called by: 
!! - main.f90
!!
      subroutine predict()
!!
      use mpi_mod
      use fileunits
      use predictionoptions 
      use nnflags 
      use globaloptions
      use symfunctions 
      use timings
      use nnewald
      use nnshort_atomic
      use nnshort_pair
      use structures
!!
      implicit none
!!
      integer zelem(max_num_atoms)                           ! internal
      integer num_atoms                                      ! internal
      integer num_pairs                                      ! internal
      integer num_atoms_element(nelem)                       ! internal 
      integer i1,i2,i3,i4,i5                                    ! internal
      integer counter                                        ! internal
      integer pairs_charge(2,max_num_pairs)                  ! internal
!!
      real*8 lattice(3,3)                                    ! internal
      real*8 xyzstruct(3,max_num_atoms)                      ! internal
      real*8 minvalue_short_atomic(nelem,maxnum_funcvalues_short_atomic)  ! internal
      real*8 maxvalue_short_atomic(nelem,maxnum_funcvalues_short_atomic)  ! internal
      real*8 avvalue_short_atomic(nelem,maxnum_funcvalues_short_atomic)   ! internal
      real*8 minvalue_elec(nelem,maxnum_funcvalues_elec)     ! internal
      real*8 maxvalue_elec(nelem,maxnum_funcvalues_elec)     ! internal
      real*8 avvalue_elec(nelem,maxnum_funcvalues_elec)      ! internal
      real*8 minvalue_short_pair(npairs,maxnum_funcvalues_short_pair)  ! internal
      real*8 maxvalue_short_pair(npairs,maxnum_funcvalues_short_pair)  ! internal
      real*8 avvalue_short_pair(npairs,maxnum_funcvalues_short_pair)   ! internal
      real*8, dimension(:,:)  , allocatable :: sens          ! internal
      real*8, dimension(:,:)  , allocatable :: sense         ! internal
      real*8 eshort                                          ! internal
      real*8 volume                                          ! internal
!! DFT data (not necessarily provided in prediction mode)
      real*8 totalcharge                                     ! internal
      real*8 totalenergy                                     ! internal
      real*8 totalforce(3,max_num_atoms)                     ! internal
      real*8 atomcharge(max_num_atoms)                       ! internal
      real*8 atomenergy(max_num_atoms)                       ! internal
!! NN data
      real*8 nntotalenergy                                   ! internal
      real*8 nnshortenergy                                   ! internal
      real*8 nntotalcharge                                   ! internal
      real*8 nnshortforce(3,max_num_atoms)                   ! internal
      real*8 nntotalforce(3,max_num_atoms)                   ! internal
      real*8 nnatomcharge(max_num_atoms)                     ! internal
      real*8 nnatomenergy(max_num_atoms)                     ! internal
      real*8 nnelecforce(3,max_num_atoms)                    ! internal
      real*8 nnpairenergy(max_num_pairs)                     ! internal
      real*8 nnstress(3,3)                                   ! internal
      real*8 nnstress_short(3,3)                             ! internal 
      real*8 nnstress_elec(3,3)                              ! internal
      real*8 nnelecenergy                                    ! internal
      real*8 atomenergysum                                   ! internal
      real*8 eshortmin                                       ! internal
      real*8 eshortmax                                       ! internal
    
      real*8 chargemin(nelem)                                ! internal
      real*8 chargemax(nelem)                                ! internal
      real*8 pressure                                        ! internal
      real*8 forcesum(3)                                     ! internal
      real*8 au2gpa                                          ! internal

!!
      character*2 elementsymbol(max_num_atoms)               ! internal
!!
      logical lperiodic                                      ! internal
!!
      call zerotime(daymode3,timemode3start,timemode3end) 
      call abstime(timemode3start,daymode3)
!!
!!=====================================================================
!! initializations 
!!=====================================================================
      if((mpirank.eq.0).and.(.not.lmd))then
        write(ounit,*)'Prediction Mode for all configurations'
      endif
      timeio                     = 0.0d0
      timeoutput                 = 0.0d0
      au2gpa=29419.844d0 ! 1 Ha/Bohr3 = 29419.844 GPa

!! we want to do the for loop from 1 to totnum_structures
      do i4 = 1,totnum_structures  
        eshort            = 0.0d0
        nnshortenergy     = 0.0d0
        nntotalenergy     = 0.0d0
        nnelecenergy      = 0.0d0
        nnatomenergy(:)   = 0.0d0
        nntotalforce(:,:) = 0.0d0
        nnshortforce(:,:) = 0.0d0
        nnelecforce(:,:)  = 0.0d0
        nntotalcharge     = 0.0d0
        atomenergysum     = 0.0d0
        nnstress_short(:,:)=0.0d0
        nnstress_elec(:,:) =0.0d0
!!
        if(lshort.and.(nn_type_short.eq.1))then
          allocate(sens(nelem,maxnum_funcvalues_short_atomic))
        elseif(lshort.and.(nn_type_short.eq.2))then
          allocate(sens(npairs,maxnum_funcvalues_short_pair))
        endif
!! we need to allocate sense also for nn_type_elec 3 and 4 because of subroutine call below
        if(lelec.and.(nn_type_elec.eq.1).or.(nn_type_elec.eq.3).or.(nn_type_elec.eq.4))then
          allocate(sense(nelem,maxnum_funcvalues_elec))
        endif
!!
        if(ldebug.and.(mpisize.gt.1).and.(.not.lmd))then
          ldebug=.false.
          write(ounit,*)'### WARNING ### ldebug is switched off for parallel case'
        endif !'
!!
!!=====================================================================
!! timining initialization of mode 3 
!!=====================================================================
        if(lfinetime)then
          dayio=0
          call abstime(timeiostart,dayio)
        endif ! lfinetime
!!
!!=====================================================================
!! read structure and derive all structure related data, also mpi distribution 
!!=====================================================================
       call getstructure_mode3(i4,num_atoms,num_pairs,zelem,&
          num_atoms_element,lattice,xyzstruct,&
          totalenergy,totalcharge,totalforce,atomenergy,atomcharge,&
          elementsymbol,lperiodic&
          )
!!
!!=====================================================================
!! read and distribute scaling data and weights, determine num_pairs if needed 
!!=====================================================================
        call initmode3(i4,&
          minvalue_short_atomic,maxvalue_short_atomic,avvalue_short_atomic,&
          minvalue_short_pair,maxvalue_short_pair,avvalue_short_pair,&
          minvalue_elec,maxvalue_elec,avvalue_elec,&
          eshortmin,eshortmax,chargemin,chargemax&
          )

        !! we should always allocate these because of the subroutine call - Emir
        allocate(nnhessian(3*num_atoms,3*num_atoms))
        allocate(nnfrequencies(3*num_atoms))
        allocate(nnnormalmodes(3*num_atoms,3*num_atoms))
        nnhessian(:,:) = 0.d0
        nnfrequencies(:) = 0.d0
        nnnormalmodes(:,:) = 0.d0
!!
!!=====================================================================
!! timining initialization of mode 3 
!!=====================================================================
        if(lfinetime)then
          call abstime(timeioend,dayio)
          timeio=timeioend-timeiostart
        endif ! lfinetime
!!
!!=====================================================================
!!=====================================================================
!! all input reading is done now
!! now predict the energies and forces etc.
!! GOAL: These subroutines should be without any IO, so that they can be
!! coupled with MD codes
!!=====================================================================
!!=====================================================================
        if(lshort)then
          if(nn_type_short.eq.1)then 
            call predictionshortatomic(&
              num_atoms,num_atoms_element,zelem,&
              lattice,xyzstruct,&
              minvalue_short_atomic,maxvalue_short_atomic,avvalue_short_atomic,&
              eshortmin,eshortmax,&
              nntotalenergy,nnshortforce,&
              nnatomenergy,nnshortenergy,nnstress_short,&
              atomenergysum,sens,lperiodic,nnhessian,nnfrequencies,nnnormalmodes)
          elseif(nn_type_short.eq.2)then
            call predictionshortpair(&
              num_atoms,num_atoms_element,zelem,&
              lattice,xyzstruct,&
              minvalue_short_pair,maxvalue_short_pair,avvalue_short_pair,&
              eshortmin,eshortmax,&
              nntotalenergy,nnshortforce,&
              nnatomenergy,nnpairenergy,nnshortenergy,&
              nnstress_short,pairs_charge,&
              atomenergysum,sens,lperiodic)
          endif
        endif ! lshort
!! electrostatic part, only if charges are not used as second output of atomic NN
        if(lelec.and.((nn_type_elec.eq.1).or.(nn_type_elec.eq.3)&
           .or.(nn_type_elec.eq.4)))then
          call predictionelectrostatic(&
            num_atoms,zelem,&
            minvalue_elec,maxvalue_elec,avvalue_elec,&
            lattice,xyzstruct,&
            nntotalcharge,nnatomcharge,&
            chargemin,chargemax,nnelecenergy,&
            nnelecforce,nnstress_elec,sense,lperiodic)
        else
!! if no electrostatics is used, initialize charge array as zero for proper output
          nnatomcharge(:)=0.0d0 
        endif ! lelec
!!
!!======================================================================
!! combine the short range and electrostatic energies
!!======================================================================
        nntotalenergy=nnshortenergy+nnelecenergy
!!
!!======================================================================
!! add energies of free atoms
!!======================================================================
        if(lremoveatomenergies.and.lshort)then
          call addatoms(num_atoms,&
            zelem,num_atoms_element,&
            atomenergysum,nnatomenergy)
          nntotalenergy=nntotalenergy+atomenergysum
        endif ! lremoveatomenergies
       
!!
!!======================================================================
!! print short range and electrostatic forces separately for debugging'
!!======================================================================
        if((mpirank.eq.0).and.(.not.lmd))then
          if(ldoforces)then
            write(ounit,*)'-------------------------------------------------------------'
            if(ldebug)then
              if(lshort)then
                write(ounit,'(A40,I10)')'NN short range forces for Configuration',i4
                do i2=1,num_atoms
                  write(ounit,'(i6,3f18.8)')i2,nnshortforce(1,i2),&
                  nnshortforce(2,i2),nnshortforce(3,i2)
                enddo
              endif
              if(lelec)then
                write(ounit,'(A42,I10)')'NN electrostatic forces for Configuration',i4
                do i2=1,num_atoms
                  write(ounit,'(i6,3f18.8)')i2,nnelecforce(1,i2),&
                  nnelecforce(2,i2),nnelecforce(3,i2)
                enddo
              endif
              write(ounit,*)'-------------------------------------------------------------'
            endif ! ' ldebug
          endif ! ldoforces
        endif !' mpirank.eq.0
!!
!!======================================================================
!! combination of short-range and electrostatic forces
!! must be done after the separate output of short and electrostatic forces
!!======================================================================
        if(ldoforces)then
          nntotalforce(:,:)=nnshortforce(:,:)+nnelecforce(:,:)
        endif ! ldoforces
!!
!!======================================================================
!! calculate the volume, needed also for stress
!!======================================================================
        if(lperiodic)then
          volume=0.0d0
          call getvolume(lattice,volume)
          if((mpirank.eq.0).and.(.not.lmd))then
            write(ounit,*)'-------------------------------------------------------------'
            write(ounit,*)'volume ',volume,' Bohr^3 for configuration ', i4
          endif ! mpirank.eq.0
        endif ! lperiodic
!!
!!======================================================================
!! combination of short-range and electrostatic stress 
!!======================================================================
        if(ldostress.and.lperiodic)then
          nnstress(:,:)=nnstress_short(:,:)+nnstress_elec(:,:)
          nnstress(:,:)=nnstress(:,:)/volume
        endif ! ldostress
!!
!!======================================================================
!! now write results to files 
!!======================================================================
        if(lfinetime)then
          dayoutput=0
          call abstime(timeoutputstart,dayoutput)
        endif ! lfinetime

        if((mpirank.eq.0).and.(.not.lmd))then
          if(i4.eq.1) then 
            open(nneunit,file='energy.out',form='formatted',status='replace')
            write(nneunit,'(2A10,3A30)')'Conf.','atoms','Ref. total energy(Ha)','NN total energy(Ha)','E(Ref.) - E(NN) (Ha/atom)'
          endif
          write(nneunit,'(2I10,3f30.8)') i4,num_atoms,totalenergy,nntotalenergy,(totalenergy-nntotalenergy)/float(num_atoms)
          if(i4.eq.totnum_structures)then
            close(nneunit)
          endif
!!
!! delete nnforces just in case an old file is present
          if(ldoforces)then
            if(i4.eq.1) then
              open(nnfunit,file='nnforces.out',form='formatted',status='unknown')
              close(nnfunit,status='delete')
              open(nnfunit,file='nnforces.out',form='formatted',status='replace')
!              write(nnfunit,*)'Configuration has',num_atoms,'atoms'
              write(nnfunit,'(2A10,6A25)')'Conf.','atom','Ref. Fx(Ha/Bohr)','Ref. Fy(Ha/Bohr)',&
                'Ref. Fz(Ha/Bohr)','NN Fx(Ha/Bohr)','NN Fy(Ha/Bohr)','NN Fz(Ha/Bohr)'
            endif
            do i3=1,num_atoms !'
              write(nnfunit,'(2I10,6f25.8)')i4,i3,&
               (totalforce(i2,i3),i2=1,3),(nntotalforce(i2,i3),i2=1,3)
            enddo 
            if(i4.eq.totnum_structures)then           
              close(nnfunit)
            endif
          endif ! ldoforces

          !! Writing Hessian matrix for each structure
          if(ldohessian.and.lwritehessian.and.(nn_type_short.eq.1))then
            if(i4.eq.1) then
              open(nnhunit,file='nnhessian.out',form='formatted',status='unknown')
              close(nnhunit,status='delete')
              open(nnhunit,file='nnhessian.out',form='formatted',status='replace')
              write(nnhunit,'(A10)')'Conf.'
            endif
            write(nnhunit,'(I10)')i4
            do i3 = 1,3*num_atoms
                write(nnhunit,'(1000f25.8)')nnhessian(i3,:)
            end do
            if(i4.eq.totnum_structures)then
              close(nnhunit)
            endif
          endif ! ldohessian

          !! Writing vibrational frequencies for each structure
          if(lcalculatefrequencies.and.(nn_type_short.eq.1))then
              if(i4.eq.1) then
                  open(nnfrequnit,file='nnfrequencies.out',form='formatted',status='unknown')
                  close(nnfrequnit,status='delete')
                  open(nnfrequnit,file='nnfrequencies.out',form='formatted',status='replace')
                  write(nnfrequnit,'(2A10,A25)')'Conf.','ID','Frequency(1/cm)'
              endif
              do i3 = 1,3*num_atoms
                  write(nnfrequnit,'(2I10,f25.8)')i4,i3,nnfrequencies(i3)
              end do
              if(i4.eq.totnum_structures)then
                  close(nnfrequnit)
              endif
          endif ! ldohessian

          if(lcalculatenormalmodes.and.lcalculatefrequencies.and.(nn_type_short.eq.1))then
              if(i4.eq.1) then
                  open(nnmodesunit,file='nnnormalmodes.out',form='formatted',status='unknown')
                  close(nnmodesunit,status='delete')
                  open(nnmodesunit,file='nnnormalmodes.out',form='formatted',status='replace')
                  write(nnmodesunit,'(A10,A25)')'Conf.'
              endif
              write(nnmodesunit,'(I10)')i4
              write(nnmodesunit,'(A10,A25)')'Freq. ID','Eigenvector'
              do i3 = 1,3*num_atoms
                  do i5 = 1,3*num_atoms
                     write(nnmodesunit,'(I10,f25.8)')i3,nnnormalmodes(i5,i3)
                  end do
              end do

              if(i4.eq.totnum_structures)then
                  close(nnmodesunit)
              endif
          endif ! ldohessian

!!======================================================================
!! check sum of forces if requested 
!!======================================================================
          if(lcheckf)then
            forcesum(:)=0.0d0
            do i3=1,num_atoms
              do i2=1,3
                forcesum(i2)=forcesum(i2)+nntotalforce(i2,i3)
              enddo ! i2
            enddo ! i3
            write(ounit,'(A10,3A25)')'Conf.','Sum of Fx(Ha/Bohr)', 'Sum of Fy(Ha/Bohr)','Sum of Fz(Ha/Bohr)'
            write(ounit,'(I10,3f25.8)')i1,forcesum(1),forcesum(2),forcesum(3)
            do i2=1,3
              if(abs(forcesum(i2)).gt.0.000001d0)then
                write(ounit,'(I10,A31,I10,f25.8)')i4,'Error in forces of component: ',&
                  i2,forcesum(i2)
                stop
              endif
            enddo ! i2
          endif ! lcheckf
!!
!! delete nnstress just in case an old file is present
          if(ldostress.and.lperiodic)then
            if(i4.eq.1)then
              open(nnsunit,file='nnstress.out',form='formatted',status='unknown')
              close(nnsunit,status='delete')
              open(nnsunit,file='nnstress.out',form='formatted',status='replace')
              write(nnsunit,'(A10,3A25)')'Conf.','NN Px(Ha/Bohr^3)','NN Py(Ha/Bohr^3)','NN Pz(Ha/Bohr^3)'
            endif
            do i1=1,3 !'
              write(nnsunit,'(I10,3f25.8)')i4,(nnstress(i1,i2),i2=1,3)
            enddo ! i1
            if(i4.eq.totnum_structures)then
              close(nnsunit) 
            endif
          endif ! ldostress
!!
!! delete nnatoms just in case an old file is present
          if(i4.eq.1)then
            open(nnaunit,file='nnatoms.out',form='formatted',status='unknown')
            close(nnaunit,status='delete')
            open(nnaunit,file='nnatoms.out',form='formatted',status='replace')
            write(nnaunit,'(2A10,A10,2A18,2A25)')'Conf.','atom','element','Ref. charge(e)',&
              'NN charge(e)','Ref. energy(Ha)','NN energy(Ha)'
          endif
          do i3=1,num_atoms
            write(nnaunit,'(2I10,I10,2f18.8,2f25.8)')i4,i3,zelem(i3),&
              atomcharge(i3),nnatomcharge(i3),atomenergy(i3),nnatomenergy(i3)
          enddo ! i3
          if(i4.eq.totnum_structures)then
            close(nnaunit)
          endif    
!!
          if(nn_type_short.eq.2)then
            if(i4.eq.1)then
              open(nnpunit,file='nnpairs.out',form='formatted',status='unknown')
              close(nnpunit,status='delete')
              open(nnpunit,file='nnpairs.out',form='formatted',status='replace')
              write(nnpunit,'(4A10,2A18,2A25)')'Conf.','atom','atom1',&
                'atom2','Ref. charge(e)','NN charge(e)','Ref. energy(Ha)','NN pair energy(Ha)'
            endif
            do i1=1,num_pairs 
              write(nnpunit,'(2I10,2A10,2f18.8,2f25.8)')i4,i1,&
                element(elementindex(pairs_charge(1,i1))),&
                element(elementindex(pairs_charge(2,i1))),&
                atomcharge(i1),0.0d0,atomenergy(i1),nnpairenergy(i1)
            enddo ! i1
            if(i4.eq.totnum_structures)then
              close(nnpunit)
            endif 
          endif   
!!
!!======================================================================
!! write output data in RuNNer format and put in all results we have
!!======================================================================
          if(i4.eq.1)then
            open(outunit,file='output.data',form='formatted',status='unknown')
            close(outunit,status='delete')
            open(outunit,file='output.data',form='formatted',status='replace')
          endif  
          write(outunit,'(a5)')'begin'
          if(lperiodic)then !'
            write(outunit,'(a7,3f14.6)')'lattice  ',(lattice(1,i1),i1=1,3)
            write(outunit,'(a7,3f14.6)')'lattice  ',(lattice(2,i1),i1=1,3)
            write(outunit,'(a7,3f14.6)')'lattice  ',(lattice(3,i1),i1=1,3)
          endif
          do i1=1,num_atoms
            write(outunit,'(a5,3f14.6,x,a2,x,5f14.6)')&
              'atom ',(xyzstruct(i2,i1),i2=1,3),&
              elementsymbol(i1),nnatomcharge(i1),nnatomenergy(i1),&
              (nntotalforce(i2,i1),i2=1,3)
          enddo
          write(outunit,'(a7,f20.6)')'energy ',nntotalenergy
          write(outunit,'(a7,f20.6)')'charge ',nntotalcharge
          write(outunit,'(a3)')'end'
          if(i4.eq.totnum_structures)then
            close(outunit)
          endif 
        endif ! mpirank.eq.0
!!
!!======================================================================
!! write results to standard out for process 0
!!======================================================================
        if((mpirank.eq.0).and.(.not.lmd))then
          write(ounit,*)'-------------------------------------------------------------'
!!          if((.not.lshort).and.(lremoveatomenergies))then
!!          write(ounit,*)'WARNING: short range energy is just sum of atomic energies because lshort=F'
          write(ounit,"(A84, I10)")'NN sum of free atom energies,short range and electrostatic energy for configuration',&
          i4
          write(ounit,'(a30,f18.8,a3)')' NN sum of free atom energies ',atomenergysum,' Ha'
          write(ounit,'(a30,f18.8,a3)')' NN short range energy        ',nntotalenergy-nnelecenergy-atomenergysum,' Ha'
          write(ounit,'(a30,f18.8,a3)')' NN electrostatic energy      ',nnelecenergy,' Ha'
          write(ounit,'(a30,f18.8,a3)')' NNenergy                     ',nntotalenergy,' Ha'
          write(ounit,*)'-------------------------------------------------------------'
          if(nn_type_short.eq.1)then
              write(ounit,*)'NN atomenergies with configuration ',i4 
            do i1=1,num_atoms
              write(ounit,'(a13,i6,x,a2,x,f14.6)')'NNatomenergy ',i1,elementsymbol(i1),&
                nnatomenergy(i1)
            enddo ! i1
          elseif(nn_type_short.eq.2)then
            write(ounit,*)'NN pairenergies with configuration',i4
            do i1=1,num_pairs
              write(ounit,'(a14,i6,x,a2,x,a2,x,f14.6)')'NNpairenergy ',i1,&
                element(elementindex(pairs_charge(1,i1))),&
                element(elementindex(pairs_charge(2,i1))),&
                nnpairenergy(i1)
            enddo ! i1
          endif
          write(ounit,*)'-------------------------------------------------------------'
          if(lelec)then
            write(ounit,*)'NNcharges for configuration ',i4
            do i1=1,num_atoms
              write(ounit,'(a10,i6,f14.8)')'NNcharge ',i1,nnatomcharge(i1)
            enddo
          endif ! 

          write(ounit,*)'-------------------------------------------------------------'
          write(ounit,*)'NN forces for the configuration ',i4
          if(ldoforces)then
            do i1=1,num_atoms
              write(ounit,'(a10,i6,3f16.8,a8)')'NNforces ',i1,nntotalforce(1,i1),&
              nntotalforce(2,i1),nntotalforce(3,i1),' Ha/Bohr'
            enddo ! i1
          write(ounit,*)'-------------------------------------------------------------'
          endif !ldoforces'
          if(ldostress.and.lperiodic)then
            write(ounit,*)'NNstress for the configuration ',i4
            do i1=1,3
              write(ounit,'(a10,3f18.8,a10)')'NNstress ',nnstress(i1,1),&
              nnstress(i1,2),nnstress(i1,3),' Ha/Bohr^3'
            enddo ! i1
            write(ounit,*)'-------------------------------------------------------------'
            pressure=(nnstress(1,1)+nnstress(2,2)+nnstress(3,3))/3.0d0
            pressure=pressure*au2gpa
            write(ounit,'(a12,f18.8,a5)')' NNpressure ',pressure,' GPa'
            write(ounit,'(a12,f18.8,a5)')' NNpressure ',pressure*10.0d0,' kbar'
            write(ounit,*)'-------------------------------------------------------------'
          endif !ldostress'
          !! write vibrational frequencies to the standard output
          if(lcalculatefrequencies.and.(nn_type_short.eq.1))then
              write(ounit,*)'NNfrequencies for the configuration ',i4
              do i1=1,3*num_atoms
                  write(ounit,'(a15,i10,f18.8)')'NNfrequency ',i1,nnfrequencies(i1)
              enddo ! i1
              write(ounit,*)'-------------------------------------------------------------'
          end if
!! sensitivity:
          if(lsens.and.lshort.and.(nn_type_short.eq.1))then
            do i1=1,nelem
              do i2=1,num_funcvalues_short_atomic(i1)
                write(ounit,'(a15,x,a2,x,i4,f16.8)')' NNsensitivity ',&
!! CHANGE ANDI: Mean square average is probably a better sensitivity value
               !element(i1),i2,sens(i1,i2)/dble(num_atoms)
                  element(i1),i2,sqrt(sens(i1,i2)/dble(num_atoms_element(i1)))
!! END CHANGE
              enddo ! i2
              write(ounit,*)'-------------------------------------------------------------'
            enddo ! i1
          endif ! lsens
          if(lsens.and.(nn_type_short.eq.2))then
            counter = 0
            do i1=1,nelem
             do i2=i1,nelem
              counter = counter + 1
              do i3=1,num_funcvalues_short_pair(counter)
                write(ounit,'(a15,x,a2,x,a2,x,i4,f16.8)')' NNsensitivity ',&
!! CHANGE ANDI: Mean square average is probably a better sensitivity value.
!!              What is the correct normalization for nn_type_short 2 (num_atoms vs. num_atoms_element(i1))?
               !element(i1),element(i2),i3,sens(counter,i3)/dble(num_atoms)
                  element(i1),element(i2),i3,sqrt(sens(counter,i3)/dble(num_atoms))
!! END CHANGE
              enddo ! i3
              write(ounit,*)'-------------------------------------------------------------'
            enddo ! i2
           enddo ! i1
          endif ! lsens
        endif ! mpirank.eq.0
!!
!! calculate final output timing
        if(lfinetime)then
          call abstime(timeoutputend,dayoutput)
          timeoutput=timeoutputend-timeoutputstart
        endif ! lfinetime
!!
!!======================================================================
!! deallocate arrays
!!======================================================================
        if(lshort.and.(nn_type_short.eq.1))then
          deallocate(sens) 
        elseif(lshort.and.(nn_type_short.eq.2))then
          deallocate(sens) 
        endif
        if(lelec.and.(nn_type_elec.eq.1).or.(nn_type_elec.eq.3).or.(nn_type_elec.eq.4))then
          deallocate(sense) 
        endif

        !! Check if Hessian and frequency arrays are allocated - Emir
        if(allocated(nnhessian))deallocate(nnhessian)
        if(allocated(nnfrequencies))deallocate(nnfrequencies)
        if(allocated(nnnormalmodes))deallocate(nnnormalmodes)

        call abstime(timemode3end,daymode3)
!!
    enddo !i4
    return 
    end 
