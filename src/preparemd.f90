!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2020 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################!!                                                                    
!! called by:                                                                                                                                                                                          
!! - predict.f90
!!
      subroutine preparemd()
!!
      use nnflags
      use globaloptions
      use fileunits
      use predictionoptions
!!
      implicit none
!!
      write(ounit,*)'ERROR: Writing nnmd.in is not yet implemented'
      stop
!!
      open(nnmdunit,file='nnmd.in',form='formatted',status='replace')
      rewind(nnmdunit)
!!
      write(nnmdunit)nn_type_short
      write(nnmdunit)lshort
      write(nnmdunit)lelec
!!
!! write short range NN parameters, atomic case
      if(lshort.and.(nn_type_short.eq.1))then

      endif
!!
!! write short range NN parameters, pair case
      if(lshort.and.(nn_type_short.eq.2))then

      endif
!!
      if(lelec.and.(nn_type_elec.eq.1))then

      endif
!!
!!
      close(nnmdunit)
!!
      return
      end 
