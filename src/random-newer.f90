


! the RND in random-new has the problem that the sequences generated with similar seeds are not independent
! also the first few numbers generated are biased if a low seed is used.
! IMPORTANT: unlike ran5 this algorithm uses a 64 bit seed (integer*8)

! use this function everywhere and then when we add another rng we only have to change the code here.
function ranx(seed) result(rnd)
    use globaloptions
    implicit none
    real*8 :: rnd
    integer*8, intent(inout) :: seed
    real*8 :: ran5, ran6
    integer :: seed4


    select case (nran)
        case (5)
            seed4 = int(seed, kind(seed4))
            rnd = ran5(seed4)
            seed = int(seed4, kind(seed))
        case (6)
            rnd = ran6(seed)
        case default
            stop 'ERROR: Unknown random number generator in ranx'
    end select

end function


function ran6(seed) result(rnd)
    implicit none
    real*8 :: rnd
    integer*8, intent(inout) :: seed
    real*8 :: xorshift64star

    rnd = xorshift64star(seed)

end function

! for conveniece.
! This function keeps track of its state by itself
function ranreal() result(rnd)
    implicit none
    real*8 :: rnd
    integer*8, save :: state = 123456789_8
    real*8 :: xorshift64star
    ! only one thread at a time. If performance is required, each thread should have its own (distinct!) seed and call rand6
    !$omp critical
    rnd = xorshift64star(state)
    !$omp end critical
end function

! uses the Box-Müller algorithm to generate normal distributed random numbers
function random_normal(seed) result (rnor)
        real*8, parameter :: PI = 4.d0 * datan(1.d0)
        integer*8, intent(inout) :: seed
        real*8 :: rnor
        real*8 :: u(2)
        real*8, save :: cache
        logical, save :: has_cache = .false.
        real*8 :: xorshift64star

        ! only one thread at a time because cache is shared between threads. If performance is required, then one should look into using the Ziggurat algorithm instead anyways.
        !$omp critical
        if (has_cache) then
            has_cache = .false.
            rnor = cache
        else
            u(1) = xorshift64star(seed)
            u(2) = xorshift64star(seed)
            rnor  = sqrt(-2.d0 * log(u(1))) * sin(2.d0 * PI * u(2))
            cache = sqrt(-2.d0 * log(u(1))) * cos(2.d0 * PI * u(2))
            has_cache = .true.
        end if
        !$omp end critical
end function random_normal



! xorshift64*
! took implementation of the algorithm in the Spire package as reference (MIT license)
! https://github.com/typelevel/spire/blob/master/extras/src/main/scala/spire/random/rng/XorShift64Star.scala
! has a period of 2**64-1
! translated to fortran by JAF
function xorshift64star(state) result(rnd)
  implicit none
  real*8 :: rnd
  integer*8, intent(inout) :: state
  ! integer(16) :: x
  ! integer*8 :: xx
  integer*8 :: b(4)
  integer*8, parameter :: f(4) = [56605, 20332, 62609, 9541] ! 2685821657736338717 in base 2**16
  integer*8 :: m(4)

  state = ieor(state, ishft(state, -12))
  state = ieor(state, ishft(state,  25))
  state = ieor(state, ishft(state, -27))

  ! fortran has no unsigned ints.
  ! below would be how we convert signed to unsigned (twos complement)
  ! if (x < 0) then
  !   x = 2_16**64 + state
  ! else
  !   x = state
  ! end if
  ! doing the modulo is equivalent however
  ! BUT: no 128 bit int support from intel compiler
  ! we therefore use some trickery by transforming the number into base 2**16
  ! the version below would work with gnu compiler
  ! x = modulo(state * 2685821657736338717_16, 2_16**64)
  ! rnd = real(x, 8) / 2_16**64
  ! write(*,*) rnd
  ! this is the version for intel
  call toBase16(state, b)
  call multBase16(b, f, m)
  ! call fromBase16(m, xx)
  ! to return a double we divide by 2^64
  rnd = m(4) / 2.d0**16 + m(3) / 2.d0**32 + m(2) / 2.d0**48 + m(1) / 2.d0**64
  ! write(*,*) rnd

end function

! becaus we cannot use 128 bit ints in ifort we represent our number using 4 ints in base 2**16
! x = sum_i b(i) * 2**(16*(i-1))
! treats x as unsigned int
subroutine toBase16(x, b)
  implicit none
  integer*8, intent(in) :: x
  integer*8, intent(out) :: b(4)
  integer :: i
  integer*8 :: t

  b(:) = 0
  if (x<0) then
    t = (9223372036854775807_8 + x) + 1
    b(4) = 2_8**15
  else
    t = x
  end if

  do i=1,4
    b(i) = b(i) + modulo(t, 2_8**16)
    t = t / 2_8**16
  end do

end subroutine

! also treats x as unsigned int
subroutine fromBase16(b, x)
  implicit none
  integer*8, intent(in) :: b(4)
  integer*8, intent(out) :: x
  integer*8 :: t
  integer :: i

  x = 0
  do i=1,3
    x = x + b(i) * 2_8**(16*(i-1))
  end do
  x = x + modulo(b(4), 2_8**16) * 2_8**(16*3)
end subroutine

! does multiplication mod 2**64 of two numbers in the base of 2**16
subroutine multBase16(a, b, c)
  implicit none
  integer*8, intent(in) :: a(4), b(4)
  integer*8, intent(out) :: c(4)

  integer :: i,j

  c(:) = 0
  do i=1,4
    do j=1,5-i
      c(i+j-1) = c(i+j-1) + a(i) * b(j)
    end do
  end do
  do i=1,3
    c(i+1) = c(i+1) + c(i) / 2_8**16
    c(i) = modulo(c(i), 2_8**16)
  end do
  c(4) = modulo(c(4), 2_8**16)

end subroutine
