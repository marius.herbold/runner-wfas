!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################

!!
!! This is a dummy routine for old random number generators in pre-release versions of RuNNer
!!
      FUNCTION ran0(seed)
      INTEGER seed 
      REAL*8 ran0
        write(*,*)'ERROR ran0'
        stop
      return
      END
!!
!!*******************************************************
!!
      FUNCTION ran1(seed)
      INTEGER seed 
      REAL*8 ran1
        write(*,*)'ERROR ran1'
        stop
      return
      END
!!
!!*******************************************************
!!
      FUNCTION ran2(seed)
      INTEGER seed 
      REAL*8 ran2
        write(*,*)'ERROR ran2'
        stop
      return
      END
!!
!!*******************************************************
!!
      FUNCTION ran3(seed)
      INTEGER seed 
      REAL*8 ran3
        write(*,*)'ERROR ran3'
        stop
      return
      END
!!
!!*******************************************************
!!


