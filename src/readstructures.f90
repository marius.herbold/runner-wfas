!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2020 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! called by:
!! - getsymmetryfunctions.f90
!! - getpairsymfunctions.f90
!!
      subroutine readstructures(npoints,num_atoms_element_list)
!!
      use fileunits
      use globaloptions
      use structures
      use nnflags
!!
      implicit none
!!
      integer npoints                                 ! in
      integer num_atoms                               ! internal
      integer zelem(max_num_atoms)                    ! internal
      integer num_atoms_element(nelem)                ! internal 
      integer num_atoms_element_list(nblock,nelem)    ! out
      integer i1                                      ! internal
!!
      real*8 totalcharge                              ! internal
      real*8 totalenergy                              ! internal
      real*8 atomcharge(max_num_atoms)                ! internal
      real*8 atomenergy(max_num_atoms)                ! internal
      real*8 xyzstruct(3,max_num_atoms)               ! internal
      real*8 totalforce(3,max_num_atoms)              ! internal
!!
      character*2 elementsymbol(max_num_atoms)        ! internal
!!
      logical lperiodic                               ! internal
!!
!! initialization
      num_atoms_element_list(:,:)=0
      originatom_id = 0
      zatom_id = 0
!!
      do i1=1,npoints
        num_atoms = 0
!!
!!      write(*,*) 'Structure', i1, ' read'
        call readonestructure(num_atoms,&
          zelem,num_atoms_element,lattice_list(1,1,i1),&
          totalcharge,totalenergy,&
          atomcharge,atomenergy,xyzstruct,&
          totalforce,elementsymbol,lperiodic,i1)
!!
        num_atoms_list(i1)          =num_atoms
        zelem_list(i1,:)            =zelem(:)
        totalcharge_list(i1)        =totalcharge
        totalenergy_list(i1)        =totalenergy
        atomcharge_list(i1,:)       =atomcharge(:)
        atomenergy_list(i1,:)       =atomenergy(:)
        xyzstruct_list(:,:,i1)      =xyzstruct(:,:)
        totalforce_list(:,:,i1)     =totalforce(:,:)
        elementsymbol_list(i1,:)    =elementsymbol(:)
        lperiodic_list(i1)          =lperiodic
        num_atoms_element_list(i1,:)=num_atoms_element(:)
!!
      enddo ! i1
!!
      
      return
      end
