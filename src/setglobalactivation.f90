!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2020 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! multipurpose subroutine
!!
!! called by:
!! - readinput.f90
!!
      subroutine setglobalactivation(ndim,counter,&
        maxnum_layers_local,maxnodes_local,nodes_local,actfunc_local,&
        actfunc_localdummy,keyword)
!!
      use fileunits
!!
      implicit none
!!
      integer i,i1,i2,i3
      integer ndim                                                  ! in
      integer maxnum_layers_local                                   ! in
      integer maxnodes_local                                        ! in
      integer nodes_local(0:maxnum_layers_local,ndim)               ! in
      integer counter                                               ! in/out
!!
      character*1 actfunc_localdummy(maxnum_layers_local)                 ! out 
      character*1 actfunc_local(maxnodes_local,maxnum_layers_local,ndim)  ! out
      character*40 dummy                                                  ! internal
      character*40 keyword                              ! in
!! 
      counter=counter+1
!!
      backspace(nnunit)
      read(nnunit,*,ERR=99)dummy,(actfunc_localdummy(i),i=1,maxnum_layers_local)
      do i1=1,maxnum_layers_local
        do i3=1,ndim
          do i2=1,nodes_local(i1,i3)
            actfunc_local(i2,i1,i3)=actfunc_localdummy(i1)
          enddo ! i2
!! fill empty part of actfunc_local
          if(nodes_local(i1,i3).lt.maxnodes_local)then
            do i2=nodes_local(i1,i3)+1,maxnodes_local
              actfunc_local(i2,i1,i3)=' '
            enddo
          endif
        enddo ! i3
      enddo ! i1
!!
      return
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
 99   continue
      write(ounit,*)'Error: keyword ',keyword
      write(ounit,*)'is missing arguments '
      stop

      end
