!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2020 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################!!
! This subroutine has been written by Jonas Finkler
! knuth shuffle implemented by JAF

subroutine shuffle(n, a, oseed)
    implicit none
    integer, intent(in) :: n
    integer, intent(inout) :: a(n)
    integer*8 :: oseed
    integer :: i, pos, tmp
    real*8 :: r
    real*8 :: ranx

    do i = n, 2, -1
        r = ranx(oseed)
        pos = int(r * i) + 1
        tmp = a(pos)
        a(pos) = a(i)
        a(i) = tmp
    end do

end subroutine shuffle
