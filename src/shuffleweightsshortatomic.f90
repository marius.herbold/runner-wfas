!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2020 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! called by:

      subroutine shuffleweightsshortatomic(sseed)

      use fileunits
      use globaloptions
      use fittingoptions
      use nnshort_atomic

      implicit none

      integer num_shuffled
      integer i1,i2
      integer*8 sseed

      real*8 ranx
      real*8 z 


      num_shuffled=0

!! loop over all weights
      do i1=1,nelem
        do i2=1,num_weights_short_atomic(i1)
!! get random number
          z=ranx(sseed)
          if(z.lt.shuffle_weights_short_atomic)then
            weights_short_atomic(i2,i1)=weights_min+(weights_max-weights_min)*ranx(sseed)
            num_shuffled=num_shuffled+1
          endif
        enddo
      enddo

      write(ounit,'(a,i5,a)')' Shuffling ',num_shuffled,' weights'
      write(ounit,*)'-------------------------------------------------------------------------------'

      return
      end
