!######################################################################
! This routine is part of
! RuNNer - RuNNer Neural Network Energy Representation
! (c) 2008-2020 Prof. Dr. Joerg Behler 
! Georg-August-Universitaet Goettingen, Germany
!
! This program is free software: you can redistribute it and/or modify it 
! under the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or 
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
! or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
! for more details.
!
! You should have received a copy of the GNU General Public License along 
! with this program. If not, see http://www.gnu.org/licenses. 
!######################################################################
!! called by:

      subroutine writefitstat_elec(ntrain,fitstatq)
!!
      use globaloptions
      use fileunits
!!
      implicit none
!!
      integer i1,i2             ! internal
      integer ntrain            ! in
      integer num_atoms         ! internal
      integer idummy
      integer fitstatq(max_num_atoms,ntrain)   ! in
!!
      real*8 edummy
!!
      write(ounit,*)'============================================================='
      write(ounit,*)'Fitting statistics:'
      write(ounit,*)'-------------------------------------------------------------'
      write(ounit,*)'Atomic charges used:'
      write(ounit,*)'           Point    Atom   Usage'
!! we use a quick and dirty way to get the number of atoms of each structure from function.data here
      open(symeunit,file='functione.data',form='formatted',status='old')
      rewind(symeunit) !'
      do i1=1,ntrain
        read(symeunit,*)num_atoms
        do i2=1,num_atoms
          read(symeunit,*)idummy
          write(ounit,'(a,5i8)')' NNstatQ ',i1,i2,fitstatq(i2,i1)
        enddo
        read(symeunit,*)edummy
      enddo ! i1
      close(symeunit)
!!
      return
      end

